﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyEffect : MonoBehaviour
{

    public GameObject ExplosionEffect;
    public AudioClip ExplosionSound;

    public void PlayExplosion()
    {
        GameObject explosion = Instantiate(ExplosionEffect, transform.position, Quaternion.identity);
        Destroy(this.gameObject, 0.15f);
        Destroy(explosion, 1.5f);
        AudioSource.PlayClipAtPoint(ExplosionSound, transform.position,1.0f);
    }
}
