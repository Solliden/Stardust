﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CallPause : MonoBehaviour
{
    GameController GameMaster;
    void Start()
    {
        GameMaster = GameObject.Find("GameMaster").GetComponent<GameController>();
    }

    public void Pause()
    {
        GameMaster.PauseGame();
    }
}
