﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CheckLevelProgress : MonoBehaviour
{
    GameController GameMaster;
    public Sprite Complete;
    public Image LevelComplete;
    public Image BonusStarCollected;
    public Image LeastMovesCompleted;
    public Text Stars;
    int CurrentLevel;
    int TotalStars = 0;
    void Start()
    {
        GameMaster = GameObject.Find("GameMaster").GetComponent<GameController>();
        CurrentLevel = GameMaster.CurrentLevelInteger;
        if (GameMaster.LevelList[CurrentLevel].LevelCompleted == true)
        {
            LevelComplete.sprite = Complete;
            TotalStars += 1;

            if (GameMaster.LevelList[CurrentLevel].BonusStarCollected == true)
            {
                BonusStarCollected.sprite = Complete;
                TotalStars += 1;
            }
            if (GameMaster.LevelList[CurrentLevel].CorrectLeastMoves == true)
            {
                LeastMovesCompleted.sprite = Complete;
                TotalStars += 1;
            }
        }
        Stars.text = TotalStars + " / 3";
    }


}
