﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UpdateLabels_Lives : MonoBehaviour
{
    public GameObject TimeLeft;
    public GameObject LivesString;
    GameController GameMaster;
    void Start()
    {
        GameMaster = GameObject.Find("GameMaster").GetComponent<GameController>();
        if (GameMaster.Lives >= 10)
        {
            TimeLeft.GetComponent<Text>().enabled = false;
        }
        else
        {
            TimeLeft.GetComponent<Text>().enabled = true;
        }
    }

    void Update()
    {
        TimeLeft.GetComponent<Text>().text = GameMaster.TimeLeft;
        LivesString.GetComponent<Text>().text = GameMaster.Lives.ToString() + " / 10";
        if (GameMaster.ActualTimeLeft > 180)
        {
            TimeLeft.GetComponent<Text>().color = new Color32(255, 0, 0, 255);
        }
        else if (GameMaster.ActualTimeLeft < 150 && GameMaster.ActualTimeLeft > 60)
        {
            TimeLeft.GetComponent<Text>().color = new Color32(255, 255, 0, 255);
        }
        else
        {
            TimeLeft.GetComponent<Text>().color = new Color32(0, 255, 0, 255);
        }
    }
}
