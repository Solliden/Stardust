﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BouncingAnimation : MonoBehaviour
{
    private float floatTimer;
    private bool goingUp = true;
    float floatSpeed = 0.35f;
    float floatRate = 1.0f;
    float rotationSpeed = 60f;

    void Update()
    {
        floatTimer += Time.deltaTime;
        Vector3 moveDir = new Vector3(0.0f, floatSpeed * Time.deltaTime, 0.0f);
        transform.Rotate(0.0f, rotationSpeed * Time.deltaTime, 0.0f);
        transform.Translate(moveDir);

        if (goingUp && floatTimer >= floatRate)
        {
            goingUp = false;
            floatTimer = 0;
            floatSpeed = -floatSpeed;
        }

        else if (!goingUp && floatTimer >= floatRate)
        {
            goingUp = true;
            floatTimer = 0;
            floatSpeed = +floatSpeed;
        }
    }
}
