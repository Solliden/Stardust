﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MuteSound : MonoBehaviour
{
    //This script should only be added on gameobjects with audio listeners which will check if they should be muted or not
    GameController GameMaster;

    void Start()
    {
        GameMaster = GameObject.Find("GameMaster").GetComponent<GameController>();
    }
    void Update()
    {
        if (GameMaster.Sound == false)
        {
            AudioListener.volume = 0.0f;
        }
        else if (GameMaster.Sound == true)
        {
            AudioListener.volume = GameMaster.Volume;
        }
    }
}
