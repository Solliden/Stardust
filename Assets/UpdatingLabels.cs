﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class UpdatingLabels
{
    Text MULabel;
    Text MDLabel;
    Text MLLabel;
    Text MRLabel;
    Text MBLabel;
    GameController GameMaster;
    LevelDataStructure CurrentLevel;
    int UpMoves;
    int DownMoves;
    int LeftMoves;
    int RightMoves;

    public void Start()
    {
        GameMaster = GameObject.Find("GameMaster").GetComponent<GameController>();
        MULabel = GameObject.Find("UpCount").GetComponent<Text>();
        MDLabel = GameObject.Find("DownCount").GetComponent<Text>();
        MLLabel = GameObject.Find("LeftCount").GetComponent<Text>();
        MRLabel = GameObject.Find("RightCount").GetComponent<Text>();
        MBLabel = GameObject.Find("BonusCount").GetComponent<Text>();
        CurrentLevel = GameMaster.CurrentLevel;
        MULabel.text = CurrentLevel.UpMoves.ToString();
        MDLabel.text = CurrentLevel.DownMoves.ToString();
        MLLabel.text = CurrentLevel.LeftMoves.ToString();
        MRLabel.text = CurrentLevel.RightMoves.ToString();
    }

    public void UpdateUpMoves(int NewMovesUp)
    {
        MULabel.text = NewMovesUp.ToString();
    }

    public void UpdateDownMoves(int NewMovesDown)
    {
        MDLabel.text = NewMovesDown.ToString();
    }

    public void UpdateLeftMoves(int NewMovesLeft)
    {
        MLLabel.text = NewMovesLeft.ToString();
    }

    public void UpdateRightMoves(int NewMovesRight)
    {
        MRLabel.text = NewMovesRight.ToString();
    }

    public void UpdateBonusMoves(int BonusMoves)
    {
        MBLabel.text = BonusMoves.ToString();
    }
}
