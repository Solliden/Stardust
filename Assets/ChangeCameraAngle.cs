﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChangeCameraAngle : MonoBehaviour
{
    GameObject MainCamera;

    private void Start()
    {
        MainCamera = GameObject.Find("Main Camera");
    }
    public void CallAngleChange()
    {
        MainCamera.GetComponent<FollowScript>().ChangeAngle();
    }
}
