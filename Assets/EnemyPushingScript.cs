﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyPushingScript : MonoBehaviour
{
    public bool ZAxis;
    public bool XAxis;
    public int OffsetRange;
    bool MovingInOneDirection = false;
    float Z_position;
    float X_position;
    public float Speed;

    void Start()
    {
        Z_position = transform.position.z;
        X_position = transform.position.x;
    }
    void Update()
    {
        if (MovingInOneDirection == false)
        {
            if (ZAxis == true)
            {
                if (transform.position.z >= Z_position - OffsetRange)
                {
                    transform.Translate(0, 0, -(Speed * Time.deltaTime));
                }
                else
                { 
                    MovingInOneDirection = true;
                }
            }
            else if (XAxis == true)
            {
                if (transform.position.x >= X_position - OffsetRange)
                {
                    transform.Translate(-(Speed * Time.deltaTime), 0,0);
                }
                else
                {
                    MovingInOneDirection = true;
                }
            }
        }
        else
        {
            if (ZAxis == true)
            {
                if (transform.position.z <= Z_position + OffsetRange)
                {
                    transform.Translate(0, 0, Speed * Time.deltaTime);
                }
                else
                {
                    MovingInOneDirection = false;
                }
            }
            else if (XAxis == true)
            {
                if (transform.position.x <= X_position + OffsetRange)
                {
                    transform.Translate(Speed * Time.deltaTime, 0, 0);
                }
                else
                {
                    MovingInOneDirection = false;
                }
            }
        }
    }
}
