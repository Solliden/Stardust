﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LoadStats : MonoBehaviour
{
    GameController GameMaster;
    public Text UpMoves;
    public Text DownMoves;
    public Text RightMoves;
    public Text LeftMoves;
    public Text TotalMoves;
    public Text LevelsCompleted;
    public Text TotalDeaths;
    public Text StarsCollected;

    private void Start()
    {
        GameMaster = GameObject.Find("GameMaster").GetComponent<GameController>();
        UpMoves.text = GameMaster.TotalMovesUp.ToString();
        DownMoves.text = GameMaster.TotalMovesDown.ToString();
        RightMoves.text = GameMaster.TotalMovesRight.ToString();
        LeftMoves.text = GameMaster.TotalMovesLeft.ToString();
        TotalMoves.text = GameMaster.TotalMoves.ToString();
        LevelsCompleted.text = GameMaster.LevelsCompleted.ToString() + " / " + "50";
        TotalDeaths.text = GameMaster.TotalDeaths.ToString();
        //StarsCollected.text


    }
}
