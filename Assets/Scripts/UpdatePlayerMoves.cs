﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UpdatePlayerMoves : MonoBehaviour {

    GameObject Player;
    Movement script;

    public void Start()
    {
        Player = GameObject.Find("Player");
        if (Player != null)
        {
            script = Player.GetComponent<Movement>();
        }
    }

    public void MoveUpUpdate()
    {
        if (script.InMotion == false)
        {
            script.MoveUp = true;
        }
    }

    public void MoveDownUpdate()
    {
        if (script.InMotion == false)
        {
            script.MoveDown = true;
        }
    }

    public void MoveLeftUpdate()
    {
        if (script.InMotion == false)
        {
            script.MoveLeft = true;
        }
    }

    public void MoveRightUpdate()
    {
        if (script.InMotion == false)
        {
            script.MoveRight = true;
        }
    }
}
