﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RestartLevel : MonoBehaviour {

    GameObject Player;
    Movement script;

    void Start()
    {
        Player = GameObject.Find("Player");

        if (Player != null)
        {
            script = Player.GetComponent<Movement>();
        }
    }

    public void ResetLevel()
    {
        script.RestartThroughButton = true;
        script.Restart();
    }
}
