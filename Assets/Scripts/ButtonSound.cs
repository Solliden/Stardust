﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ButtonSound : MonoBehaviour {

    GameObject GameMaster;

    public void PlayButtonSound()
    {
        GameMaster = GameObject.Find("GameMaster");
        GameMaster.GetComponent<GameController>().PlayButtonSound();
    }
}
