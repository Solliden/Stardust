﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.SceneManagement;

public class PlayerController : MonoBehaviour
{
    GameObject Player;
    GameObject GameMaster;
    private bool isObjectClicked = false;
    Vector3 Target;
    Color ObjectColor;
    bool Moving;
    RaycastHit hit;
    Vector3 ObjectPosition;
    GameObject ObjectHit;
    bool ObjectSelected;
    float DistanceTravelled;
    Vector3 LastPlayerPosition;
    enum Direction { UP, DOWN, LEFT, RIGHT };
    enum Reason { GREEN, NOMOVES, WRONGAXIS };
    int CurrentDirection;
    int ChangeColour;
    LevelDataStructure CurrentLevel;
    int MoveUpAmount = 0;
    int MoveDownAmount = 0;
    int MoveLeftAmount = 0;
    int MoveRightAmount = 0;
    bool FallingTarget = false;
    TextMeshProUGUI MULabel;
    TextMeshProUGUI MDLabel;
    TextMeshProUGUI MLLabel;
    TextMeshProUGUI MRLabel;
    bool AlreadyRestarting = false;
    public AudioSource WinningSound;

    void Start()
    {
        Player = GameObject.Find("Player");
        GameMaster = GameObject.Find("GameMaster");

        if (GameMaster != null)
        {
            CurrentLevel = GameMaster.GetComponent<GameController>().CurrentLevel;
        }
        MoveUpAmount = CurrentLevel.UpMoves;
        MoveDownAmount = CurrentLevel.DownMoves;
        MoveLeftAmount = CurrentLevel.LeftMoves;
        MoveRightAmount = CurrentLevel.RightMoves;
        MULabel = GameObject.Find("Up").GetComponent<TextMeshProUGUI>();
        MDLabel = GameObject.Find("Down").GetComponent<TextMeshProUGUI>();
        MLLabel = GameObject.Find("Left").GetComponent<TextMeshProUGUI>();
        MRLabel = GameObject.Find("Right").GetComponent<TextMeshProUGUI>();
        MULabel.text = "Moves Up: " + MoveUpAmount.ToString();
        MDLabel.text = "Moves Down: " + MoveDownAmount.ToString();
        MLLabel.text = "Moves Left: " + MoveLeftAmount.ToString();
        MRLabel.text = "Moves Right: " + MoveRightAmount.ToString();
    }

    void Update()
    {

        if (Player.transform.position.y <= -2 && AlreadyRestarting == false)
        {
            Restart();
        }

        RaycastHit CheckFloor;
        if (!Physics.Raycast(Player.transform.position, new Vector3(0, -1, 0), out CheckFloor, Mathf.Infinity) && FallingTarget == false)
        {
            SetFinalTarget();
            FallingTarget = true;
            Moving = true;
        }

        if (Input.GetMouseButtonDown(0) && Moving == false && ObjectSelected == false && FallingTarget == false)
        {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

            if (Physics.Raycast(ray, out hit, 100))
            {
                if (hit.transform.gameObject.name.Contains("Block") == true)
                {

                    ObjectSelected = true;
                    ObjectPosition = hit.transform.position;
                    ObjectHit = hit.transform.gameObject;
                    ObjectColor = ObjectHit.GetComponent<Renderer>().material.color;

                    if (Player.transform.position.x != ObjectHit.transform.position.x && Player.transform.position.z != ObjectHit.transform.position.z)
                    {
                        ChangeColour = (int)Reason.WRONGAXIS;
                        UpdateObjectColour(ChangeColour);
                    }
                    else
                    {
                        CheckDirection();
                        if (EnoughMoves() == true)
                        {
                            Moving = true;
                            isObjectClicked = true;
                            Target = new Vector3(ObjectPosition.x, ObjectPosition.y + 1.0f, ObjectPosition.z);
                            ChangeColour = (int)Reason.GREEN;
                            UpdateObjectColour(ChangeColour);
                            DistanceTravelled = 1.0f;
                        }
                    }
                }
            }
        }

        if (isObjectClicked == true)
        {
            LastPlayerPosition = Player.transform.position;
            Player.transform.position = Vector3.MoveTowards(Player.transform.position, Target, Time.deltaTime * 4);
            DistanceTravelled += Vector3.Distance(Player.transform.position, LastPlayerPosition);
            if (DistanceTravelled >= 1)
            {
                if (!(Player.transform.position == Target))
                {
                    DistanceTravelled -= 1;
                    DeductMoveCount();
                }
            }

            if (Player.transform.position == Target)
            {
                ObjectHit.GetComponent<Renderer>().material.color = ObjectColor;
                DistanceTravelled = 0;
                isObjectClicked = false;
                Moving = false;
                ObjectSelected = false;

                if (FallingTarget == true)
                {
                    Player.transform.GetComponent<Rigidbody>().velocity = new Vector3(0, -5, 0);

                }

            }
        }
    }

    IEnumerator ResetColour()
    {
        yield return new WaitForSeconds(0.25f);
        ObjectHit.GetComponent<Renderer>().material.color = ObjectColor;
        ObjectSelected = false;
    }

    void CheckDirection()
    {
        if (Player.transform.position.x > ObjectPosition.x)
        {
            CurrentDirection = (int)Direction.UP;
        }
        else if (Player.transform.position.x < ObjectPosition.x)
        {
            CurrentDirection = (int)Direction.DOWN;
        }
        else if (Player.transform.position.z > ObjectPosition.z)
        {
            CurrentDirection = (int)Direction.LEFT;
        }
        else if (Player.transform.position.z < ObjectPosition.z)
        {
            CurrentDirection = (int)Direction.RIGHT;
        }
    }

    void DeductMoveCount()
    {
        if (CurrentDirection == (int)Direction.UP)
        {
            MoveUpAmount -= 1;
            MULabel.text = "Moves Up: " + MoveUpAmount.ToString();
            if (EnoughMoves() == false)
            {
                SetFinalTarget();
            }
        }
        else if (CurrentDirection == (int)Direction.DOWN)
        {
            MoveDownAmount -= 1;
            MDLabel.text = "Moves Down: " + MoveDownAmount.ToString();
            if (EnoughMoves() == false)
            {
                SetFinalTarget();
            }
        }
        else if (CurrentDirection == (int)Direction.LEFT)
        {
            MoveLeftAmount -= 1;
            MLLabel.text = "Moves Left: " + MoveLeftAmount.ToString();
            if (EnoughMoves() == false)
            {
                SetFinalTarget();
            }
        }
        else if (CurrentDirection == (int)Direction.RIGHT)
        {
            MoveRightAmount -= 1;
            MRLabel.text = "Moves Right: " + MoveRightAmount.ToString();
            if (EnoughMoves() == false)
            {
                SetFinalTarget();
            }
        }
    }

    void UpdateObjectColour(int ChangeColour)
    {
        if ((Reason)ChangeColour == Reason.GREEN)
        {
            ObjectHit.GetComponent<Renderer>().material.color = new Color32(124, 252, 0, 255);
        }
        else if ((Reason)ChangeColour == Reason.NOMOVES)
        {
            Debug.Log("No Moves");
            ObjectHit.GetComponent<Renderer>().material.color = new Color32(255, 0, 0, 255);
            StartCoroutine(ResetColour());
        }
        else if ((Reason)ChangeColour == Reason.WRONGAXIS)
        {
            Debug.Log("Wrong Axis");
            ObjectHit.GetComponent<Renderer>().material.color = new Color32(255, 0, 0, 255);
            StartCoroutine(ResetColour());
        }
    }

    bool EnoughMoves()
    {
        if (MoveUpAmount <= 0 && CurrentDirection == (int)Direction.UP)
        {
            if (Moving == false)
            {
                ChangeColour = (int)Reason.NOMOVES;
                UpdateObjectColour(ChangeColour);
            }
            return false;
        }
        if (MoveDownAmount <= 0 && CurrentDirection == (int)Direction.DOWN)
        {
            if (Moving == false)
            {
                ChangeColour = (int)Reason.NOMOVES;
                UpdateObjectColour(ChangeColour);
            }
            return false;
        }
        if (MoveLeftAmount <= 0 && CurrentDirection == (int)Direction.LEFT)
        {
            if (Moving == false)
            {
                ChangeColour = (int)Reason.NOMOVES;
                UpdateObjectColour(ChangeColour);
            }
            return false;
        }
        if (MoveRightAmount <= 0 && CurrentDirection == (int)Direction.RIGHT)
        {
            if (Moving == false)
            {
                ChangeColour = (int)Reason.NOMOVES;
                UpdateObjectColour(ChangeColour);
            }
            return false;
        }
        return true;
    }

    void SetFinalTarget()
    {
        if ((Direction)CurrentDirection == Direction.UP)
        {
            Target = new Vector3(Player.transform.position.x - 1 + DistanceTravelled, Player.transform.position.y, Player.transform.position.z);
        }
        else if ((Direction)CurrentDirection == Direction.DOWN)
        {
            Target = new Vector3(Player.transform.position.x + 1 - DistanceTravelled, Player.transform.position.y, Player.transform.position.z);
        }
        else if ((Direction)CurrentDirection == Direction.LEFT)
        {
            Target = new Vector3(Player.transform.position.x, Player.transform.position.y, Player.transform.position.z - 1 + DistanceTravelled);
        }
        else if ((Direction)CurrentDirection == Direction.RIGHT)
        {
            Target = new Vector3(Player.transform.position.x, Player.transform.position.y, Player.transform.position.z + 1 - DistanceTravelled);
        }
    }

    public void Restart()
    {
        AlreadyRestarting = true;
        StartCoroutine(RestartDelay());

    }

    IEnumerator RestartDelay()
    {
        yield return new WaitForSeconds(0.2f);
        Scene CurrentScene = SceneManager.GetActiveScene();
        ChangeScene(CurrentScene.name);
    }

    void ChangeScene(string SceneName)
    {
        SceneManager.LoadScene(SceneName, LoadSceneMode.Single);
    }

    public void ObjectTouched(string ObjectName)
    {
        if (ObjectName == "WinObject")
        {
            Destroy(GameObject.Find("WinObject"));
            StartCoroutine(LevelCompleted());
        }
    }

    IEnumerator LevelCompleted()
    {
        WinningSound.Play();
        yield return new WaitForSeconds(2.0f);
        ChangeScene("LevelSelect");
    }
}
