﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using System;

public class ChangeLevel : MonoBehaviour {

    GameObject PauseCanvas;
    GameObject GameMaster;
    public LevelData LevelDataScript = new LevelData();
    string SceneName;
    string TemporaryIntegerString = "";
    int CurrentLevel;
    public void UpdateSceneName(string newName)
    {
        SceneName = newName;
    }
    public void LoadLevel()
    {
        GameMaster = GameObject.Find("GameMaster");

        if (GameMaster != null)
        {
            GameMaster.GetComponent<GameController>().SetCurrentLevel(LevelDataScript.GetType().GetField(SceneName).GetValue(LevelDataScript) as LevelDataStructure);

            for (int i = 0; i < SceneName.Length; i++)
            {
                if (Char.IsDigit(SceneName[i]))
                {
                    TemporaryIntegerString += SceneName[i]; // Adds integers from the string such as "Level21", equalling a string -> "21"
                }
            }
            if (TemporaryIntegerString.Length > 0)
            {
                CurrentLevel = int.Parse(TemporaryIntegerString); // Converts string -> "21" to an integer 21
                GameMaster.GetComponent<GameController>().CurrentLevelInteger = CurrentLevel;
            }

        }

        GameMaster.GetComponent<GameController>().DeductLives();

        SceneManager.LoadScene(SceneName, LoadSceneMode.Single);
        PauseCanvas = GameObject.Find("PauseMenu");
        if (PauseCanvas != null)
        {
            PauseCanvas.GetComponent<Canvas>().enabled = false;
            //Time.timeScale = 1.0f;
        }
    }

}
