﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PauseGame : MonoBehaviour {

    GameObject PauseMenu;

    void Start ()
    {
        PauseMenu = GameObject.Find("PauseMenu");
    }

    public void Pause()
    {
        if (PauseMenu.GetComponent<Canvas>().enabled == false)
        {
            Time.timeScale = 0.0f;
            PauseMenu.GetComponent<Canvas>().enabled = true;

        }
        else if (PauseMenu.GetComponent<Canvas>().enabled == true)
        {
            Time.timeScale = 1.0f;
            PauseMenu.GetComponent<Canvas>().enabled = false;

        }
    }
}
