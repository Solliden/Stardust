﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SinglePauseScreen : MonoBehaviour { 

public static SinglePauseScreen single;

    void Awake()
    {
        if (single == null)
        {
            DontDestroyOnLoad(gameObject);
            single = this;
        }
        else if (single != this)
        {
            Destroy(gameObject);
        }
    }

}
