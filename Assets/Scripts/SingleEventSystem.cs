﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SingleEventSystem : MonoBehaviour {

    public static SingleEventSystem single;

    void Awake()
    {
        if (single == null)
        {
            DontDestroyOnLoad(gameObject);
            single = this;
        }
        else if (single != this)
        {
            Destroy(gameObject);
        }

    }
}
