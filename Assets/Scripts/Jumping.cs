﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Jumping : MonoBehaviour {

    protected float Animation;
    bool MoveObject = false;
    Vector3 CurrentPosition;


	void Update () {

        if (Input.GetKeyDown(KeyCode.RightArrow))
        {
            MoveObject = true;
            CurrentPosition = transform.position;
            Animation = 0;
            Debug.Log(transform.position.z);
        }

        if (MoveObject == true)
        {
            Animation += Time.deltaTime;

            Animation = Animation % 5;

            transform.position = MathParabola.Parabola(CurrentPosition, new Vector3(CurrentPosition.x, CurrentPosition.y, CurrentPosition.z + 2), 1f, Animation / 2.0f);
        }

        if (transform.position.z >= CurrentPosition.z + 1.995f && MoveObject == true)
        {
            transform.position = Vector3.MoveTowards(transform.position, new Vector3(transform.position.x, transform.position.y, CurrentPosition.z + 2.0f),1.0f);
            Debug.Log("passed");
            MoveObject = false;
        }

	}
}
