﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShowStatistics : MonoBehaviour {

    public GameObject MainCanvas;
    public Canvas Statistics;

    public void ShowStats()
    {
        if (Statistics.GetComponent<Canvas>().enabled == false)
        {
            MainCanvas.SetActive(false);
            Statistics.GetComponent<Canvas>().enabled = true;
        }
        else if (Statistics.GetComponent<Canvas>().enabled == true)
        {
            MainCanvas.SetActive(true);
            Statistics.GetComponent<Canvas>().enabled = false;
        }

    }
}
