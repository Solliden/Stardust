﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowScript : MonoBehaviour {

    GameObject player;
    Vector3 CameraOffset;
    int CurrentAngle = 0;
    float CurrentZoom = 0;
    bool PlayerFalling = false;
    bool Rotating = false;
    Vector3 NewCameraPosition;
    Quaternion NewCameraRotation;
    Vector3 PlayerPosition;
    Vector3 WinObjectPosition;
    Quaternion PlayerRotation;
    float CameraSpeed = 2.0f;
    int MaxZoomValue = 3; // CHANGE THIS if higher zoom level needed
    enum Angle { UP , LEFT, DOWN, RIGHT };

    GameObject WinObject;
    bool LevelStartAnimation = true;
    bool LowerCameraAnimation = false;
    void Start()
    {
        player = GameObject.Find("Player");
        WinObject = GameObject.Find("WinObject");
        CameraOffset = transform.position;
        PlayerPosition = player.transform.transform.position;
        WinObjectPosition = WinObject.transform.transform.position;
        transform.position = WinObjectPosition + new Vector3(transform.position.x,transform.position.y / 2,transform.position.z / 2);
        StartCoroutine(StartAnimation());
    }
    void Update()
    {
        if (LowerCameraAnimation == true)
        {
            transform.position = Vector3.Slerp(transform.position, new Vector3((CameraOffset.x / 1.35f) + PlayerPosition.x, 1.75f, (CameraOffset.z / 1.35f) + PlayerPosition.z), Time.deltaTime * (CameraSpeed / 3)); //Slower camera move
            transform.LookAt(player.transform.transform.position);
        }
        else if (LevelStartAnimation == false)
        {
            if (Input.GetKeyDown(KeyCode.Space))
            {
                ChangeZoom();
            }
            else if (Input.GetKeyDown(KeyCode.E))
            {
                ChangeAngle();
            }
            PlayerPosition = player.transform.transform.position;
            PlayerRotation = player.transform.transform.rotation;
            PlayerFalling = player.GetComponent<Motion>().Falling; //Check if player is falling, and if he is, dont follow the player with the camera
            if (PlayerFalling == false)
            {
                transform.position = Vector3.Slerp(transform.position, CameraOffset + PlayerPosition, Time.deltaTime * CameraSpeed);
                transform.rotation = Quaternion.Slerp(transform.rotation, NewCameraRotation, Time.deltaTime * CameraSpeed);
                if (Rotating == true)
                {
                    if (Vector3.Distance(transform.position, CameraOffset + PlayerPosition) < 0.75f) // Adjust for how frequently a user can spam the button
                    {
                        Rotating = false;
                    }
                }
            }
        }
    }

    public void ChangeAngle()
    {
        if (Rotating == false && PlayerFalling == false)
        {
            Rotating = true;
            CurrentAngle += 1;

            if (CurrentAngle >= 4)
            {
                CurrentAngle = 0;
            }
            switch (CurrentAngle)
            {
                case 0:
                    CameraOffset = new Vector3(0, CameraOffset.y, (-CameraOffset.x));
                    NewCameraRotation = Quaternion.Euler(45, 360, 0);
                    break;
                case 1:
                    CameraOffset = new Vector3(CameraOffset.z, CameraOffset.y, 0);
                    NewCameraRotation = Quaternion.Euler(45, 90, 0);
                    break;
                case 2:
                    CameraOffset = new Vector3(0, CameraOffset.y, (-CameraOffset.x));
                    NewCameraRotation = Quaternion.Euler(45, 180, 0);
                    break;
                case 3:
                    CameraOffset = new Vector3(CameraOffset.z, CameraOffset.y, 0);
                    NewCameraRotation = Quaternion.Euler(45, 270, 0);
                    break;
            }
        }
    }

    public void ChangeZoom()
    {

        if (PlayerFalling == false)
        {
            CurrentZoom += 1;
            switch (CurrentAngle)
            {
                case 0:
                    if (CurrentZoom == MaxZoomValue)
                    {
                        CameraOffset = CameraOffset - new Vector3(0, CurrentZoom - 1, (-CurrentZoom + 1) / 2);
                    }
                    else
                    {
                        CameraOffset = CameraOffset + new Vector3(0, 1, -0.5f);
                    }
                    break;
                case 1:
                    if (CurrentZoom == MaxZoomValue)
                    {
                        CameraOffset = CameraOffset - new Vector3((-CurrentZoom + 1) / 2, CurrentZoom - 1, 0);
                    }
                    else
                    {
                        CameraOffset = CameraOffset + new Vector3(-0.5f, 1, 0);
                    }
                    break;
                case 2:
                    if (CurrentZoom == MaxZoomValue)
                    {
                        CameraOffset = CameraOffset - new Vector3(0, CurrentZoom - 1, (CurrentZoom - 1) / 2);
                    }
                    else
                    {
                        CameraOffset = CameraOffset + new Vector3(0, 1, 0.5f);
                    }

                    break;
                case 3:
                    if (CurrentZoom == MaxZoomValue)
                    {
                        CameraOffset = CameraOffset - new Vector3((CurrentZoom - 1) / 2, CurrentZoom - 1, 0);
                    }
                    else
                    {
                        CameraOffset = CameraOffset + new Vector3(0.5f, 1, 0);
                    }
                    break;
            }

            if (CurrentZoom >= MaxZoomValue)
            {
                CurrentZoom = 0; //Reset zoom level
            }
        }
    }

    IEnumerator StartAnimation()
    {
        yield return new WaitForSeconds(1.25f);
        LevelStartAnimation = false;
    }
    public void LowerCameraLevelEnd()
    {
        LowerCameraAnimation = true;
    }
}
