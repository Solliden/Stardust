﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

[System.Serializable]
public class Movement : MonoBehaviour {

    GameObject GameControllerObject;
    GameController GameControllerScript;
    Rigidbody rb;
    public bool MoveUp = false;
    public bool MoveDown = false;
    public bool MoveLeft = false;
    public bool MoveRight = false;
    int speed = 4;
    public bool InMotion = false;
    Vector3 target;
    bool TargetSet = false;
    public int MoveUpAmount = 0;
    public int MoveDownAmount = 0;
    public int MoveLeftAmount = 0;
    public int MoveRightAmount = 0;
    Text MULabel;
    Text MDLabel;
    Text MLLabel;
    Text MRLabel;
    Button MUButton;
    Button MDButton;
    Button MLButton;
    Button MRButton;
    Vector3 PlayerStartPosition;
    Vector3 CurrentPosition;
    Rigidbody PlayerRB;
    bool RestartInProgress = false;
    bool GameWon = false;
    AudioSource FallingSound;
    public bool RestartThroughButton = false;
    GameObject GameMaster;
    public LevelDataStructure CurrentLevel;
    bool Jumping = false;
    protected float Animation;
    bool CurrentlyMoving = false;
    bool TargetForJumping = false;
    bool PositionUpdated = false;
    public AudioSource WinningSound;

    void Start()
    {
        rb = GetComponent<Rigidbody>();
        GameControllerObject = GameObject.Find("GameMaster");
        GameControllerScript = GameControllerObject.GetComponent<GameController>();
        GameMaster = GameObject.Find("GameMaster");
        if (GameMaster != null)
        {
            CurrentLevel = GameMaster.GetComponent<GameController>().CurrentLevel;
        }
        MULabel = GameObject.Find("MoveUpCounter").GetComponent<Text>();
        MDLabel = GameObject.Find("MoveDownCounter").GetComponent<Text>();
        MLLabel = GameObject.Find("MoveLeftCounter").GetComponent<Text>();
        MRLabel = GameObject.Find("MoveRightCounter").GetComponent<Text>();
        MRButton = GameObject.Find("MoveRight").GetComponent<Button>();
        MDButton = GameObject.Find("MoveDown").GetComponent<Button>();
        MUButton = GameObject.Find("MoveUp").GetComponent<Button>();
        MLButton = GameObject.Find("MoveLeft").GetComponent<Button>();
        MoveUpAmount = CurrentLevel.UpMoves;
        MoveDownAmount = CurrentLevel.DownMoves;
        MoveLeftAmount = CurrentLevel.LeftMoves;
        MoveRightAmount = CurrentLevel.RightMoves;
        MULabel.text = MoveUpAmount.ToString();
        MDLabel.text = MoveDownAmount.ToString();
        MLLabel.text = MoveLeftAmount.ToString();
        MRLabel.text = MoveRightAmount.ToString();
        PlayerStartPosition = transform.position;
    }


    void Update()
    {

        if (transform.position.y <= -2 && GameWon == false)
        {
            Restart();
        }

        if (Input.GetKeyDown(KeyCode.UpArrow) && InMotion == false && MoveUpAmount > 0)
        {
            MoveUp = true;
            InMotion = true;
            GameMaster.GetComponent<GameController>().PlayButtonSound();
        }
        if (Input.GetKeyDown(KeyCode.DownArrow) && InMotion == false && MoveDownAmount > 0)
        {
            MoveDown = true;
            InMotion = true;
            GameMaster.GetComponent<GameController>().PlayButtonSound();
        }
        if (Input.GetKeyDown(KeyCode.LeftArrow) && InMotion == false && MoveLeftAmount > 0)
        {
            MoveLeft = true;
            InMotion = true;
            GameMaster.GetComponent<GameController>().PlayButtonSound();
        }
        if (Input.GetKeyDown(KeyCode.RightArrow) && InMotion == false  && MoveRightAmount > 0)
        {
            MoveRight = true;
            InMotion = true;
            GameMaster.GetComponent<GameController>().PlayButtonSound();
        }

        if (MoveUp == true)
        {
            InMotion = true;
            if (PositionUpdated == false)
            {
                CurrentPosition = transform.position;
                Animation = 0.0f;
                PositionUpdated = true;
            }
            if (TargetSet == false)
            {
                MoveUpAmount -= 1;
                MULabel.text = MoveUpAmount.ToString();
                if (MoveUpAmount <= 0)
                {
                    MUButton.interactable = false;
                    CheckForRestart();
                }
                if (Jumping == true && CurrentlyMoving == false)
                {
                    TargetForJumping = true;
                    target = new Vector3(transform.position.x - 2.0f,transform.position.y,transform.position.z);
                }
                else
                {
                    target = new Vector3(transform.position.x - 1.0f, transform.position.y, transform.position.z);
                    CurrentlyMoving = true;
                }
                TargetSet = true;
            }

            if (transform.position != target)
            {
                transform.Rotate(0, 0, 5);
                if (Jumping == true && CurrentlyMoving == false && transform.position.x > target.x + 0.1f)
                {
                    Animation += Time.deltaTime;

                    Animation = Animation % 5;

                    transform.position = MathParabola.Parabola(CurrentPosition, new Vector3(target.x, target.y, target.z), 1.0f, Animation / 0.8f);
                }
                else if (Jumping == true && CurrentlyMoving == false && transform.position.x < target.x + 0.1f && transform.position != target)
                {
                    transform.position = Vector3.MoveTowards(transform.position, target, 1.0f);
                }
                else
                {
                    transform.position = Vector3.MoveTowards(transform.position, target, Time.deltaTime * speed);
                }

            }
            else
            {
                TargetSet = false;
                MoveUp = false;
                if (CurrentlyMoving == false && TargetForJumping == false)
                {
                    Jumping = false;
                }
                CurrentlyMoving = false;
                PositionUpdated = false;
                GameControllerScript.TotalMovesUp += 1;
                GameControllerScript.TotalMoves += 1;
                //GameControllerScript.Save();
                StartCoroutine(DelayInput());
            }
        }

        if (MoveDown == true)
        {
            InMotion = true;
            if (PositionUpdated == false)
            {
                CurrentPosition = transform.position;
                Animation = 0.0f;
                PositionUpdated = true;
            }
            if (TargetSet == false)
            {
                MoveDownAmount -= 1;
                MDLabel.text = MoveDownAmount.ToString();
                if (MoveDownAmount <= 0)
                {
                    MDButton.interactable = false;
                    CheckForRestart();
                }
                if (Jumping == true && CurrentlyMoving == false)
                {
                    TargetForJumping = true;
                    target = new Vector3(transform.position.x + 2.0f, transform.position.y, transform.position.z);
                }
                else
                {
                    target = new Vector3(transform.position.x + 1.0f, transform.position.y, transform.position.z);
                    CurrentlyMoving = true;
                }
                TargetSet = true;
            }

            if (transform.position != target)
            {
                transform.Rotate(0, 0, -5);
                if (Jumping == true && CurrentlyMoving == false && transform.position.x < target.x - 0.1f)
                {
                    Animation += Time.deltaTime;

                    Animation = Animation % 5;

                    transform.position = MathParabola.Parabola(CurrentPosition, new Vector3(target.x, target.y, target.z), 1.0f, Animation / 0.8f);
                }
                else if (Jumping == true && CurrentlyMoving == false && transform.position.x > target.x - 0.1f && transform.position != target)
                {
                    transform.position = Vector3.MoveTowards(transform.position, target, 1.0f);
                }
                else
                {
                    transform.position = Vector3.MoveTowards(transform.position, target, Time.deltaTime * speed);
                }

            }
            else
            {
                GameControllerScript.TotalMovesDown += 1;
                GameControllerScript.TotalMoves += 1;
                //GameControllerScript.Save();
                TargetSet = false;
                MoveDown = false;
                if (CurrentlyMoving == false && TargetForJumping == false)
                {
                    Jumping = false;
                }
                CurrentlyMoving = false;
                PositionUpdated = false;
                StartCoroutine(DelayInput());
            }
        }

        if (MoveLeft == true)
        {
            InMotion = true;
            if (PositionUpdated == false)
            {
                CurrentPosition = transform.position;
                Animation = 0.0f;
                PositionUpdated = true;
            }
            if (TargetSet == false)
            {
                MoveLeftAmount -= 1;
                MLLabel.text = MoveLeftAmount.ToString();
                if (MoveLeftAmount <= 0)
                {
                    MLButton.interactable = false;
                    CheckForRestart();
                }
                if (Jumping == true && CurrentlyMoving == false)
                {
                    TargetForJumping = true;
                    target = new Vector3(transform.position.x, transform.position.y, transform.position.z - 2.0f);
                }
                else
                {
                    target = new Vector3(transform.position.x, transform.position.y, transform.position.z - 1.0f);
                    CurrentlyMoving = true;
                }
                TargetSet = true;
            }

            if (transform.position != target)
            {
                transform.Rotate(-5, 0, 0);
                if (Jumping == true && CurrentlyMoving == false && transform.position.z > target.z + 0.1f)
                {
                    Animation += Time.deltaTime;

                    Animation = Animation % 5;

                    transform.position = MathParabola.Parabola(CurrentPosition, new Vector3(target.x, target.y, target.z), 1.0f, Animation / 0.8f);
                }
                else if (Jumping == true && CurrentlyMoving == false && transform.position.z < target.z - 0.1f && transform.position != target)
                {
                    transform.position = Vector3.MoveTowards(transform.position, target, 1.0f);
                }
                else
                {
                    transform.position = Vector3.MoveTowards(transform.position, target, Time.deltaTime * speed);
                }

            }
            else
            {
                GameControllerScript.TotalMovesLeft += 1;
                GameControllerScript.TotalMoves += 1;
                //GameControllerScript.Save();
                TargetSet = false;
                MoveLeft = false;
                if (CurrentlyMoving == false && TargetForJumping == false)
                {
                    Jumping = false;
                }
                CurrentlyMoving = false;
                PositionUpdated = false;
                StartCoroutine(DelayInput());
            }
        }

        if (MoveRight == true)
        {
            InMotion = true;
            if (PositionUpdated == false)
            {
                CurrentPosition = transform.position;
                Animation = 0.0f;
                PositionUpdated = true;
            }
            if (TargetSet == false)
            {
                MoveRightAmount -= 1;
                MRLabel.text = MoveRightAmount.ToString();
                if (MoveRightAmount <= 0)
                {
                    MRButton.interactable = false;
                    CheckForRestart();
                }
                if (Jumping == true && CurrentlyMoving == false)
                {
                    TargetForJumping = true;
                    target = new Vector3(transform.position.x, transform.position.y, transform.position.z + 2.0f);
                }
                else
                {
                    target = new Vector3(transform.position.x, transform.position.y, transform.position.z + 1.0f);
                    CurrentlyMoving = true;
                }
                TargetSet = true;
            }

            if (transform.position != target)
            {
                transform.Rotate(5, 0, 0);

                if (Jumping == true && CurrentlyMoving == false && transform.position.z < target.z - 0.1f)
                {
                    Animation += Time.deltaTime;

                    Animation = Animation % 5;

                    transform.position = MathParabola.Parabola(CurrentPosition, new Vector3(target.x, target.y, target.z), 1.0f, Animation / 0.8f);
                }
                else if (Jumping == true && CurrentlyMoving == false && transform.position.x < target.x + 0.1f && transform.position != target)
                {
                    transform.position = Vector3.MoveTowards(transform.position, target, 1.0f);
                }
                else
                {
                    transform.position = Vector3.MoveTowards(transform.position, target, Time.deltaTime * speed);
                }

            }
            else
            {
                GameControllerScript.TotalMovesRight += 1;
                GameControllerScript.TotalMoves += 1;
                //GameControllerScript.Save();
                TargetSet = false;
                MoveRight = false;
                if (CurrentlyMoving == false && TargetForJumping == false)
                {
                    Jumping = false;
                }
                CurrentlyMoving = false;
                PositionUpdated = false;
                StartCoroutine(DelayInput());
            }
        }

    }

    void OnTriggerEnter(Collider other)
    {
        Debug.Log(other);
        if (other.name == "JumpBlock")
        {
            Jumping = true;
        }
        if (other.name != "WinObject" && other.name != "JumpBlock")
        {
            Debug.Log("Passed");
            Jumping = false;
        }
        if (other.name == "WinObject")
        {
            Destroy(other.gameObject);
            GameWon = true;
            InMotion = true;
            LevelCompleted();
        }
    }

    void LevelCompleted()
    {
        StartCoroutine(UnloadLevel());
    }

    void CheckForRestart()
    {
        if (MoveUpAmount <= 0 && MoveDownAmount <= 0 && MoveLeftAmount <= 0 && MoveRightAmount <= 0)
        {
            StartCoroutine(InitiateRestarting());
        }
    }

    public void Restart()
    {
        if (RestartInProgress == false)
        {
            Debug.Log("Passed");
            if (RestartThroughButton == false)
            {
                FallingSound = GetComponent<AudioSource>();
                FallingSound.Play();
            }
            GameControllerScript.TotalDeaths += 1;
            //GameControllerScript.Save();
            RestartInProgress = true;
            StartCoroutine(RestartGame());
        }
    }

    IEnumerator RestartGame()
    {
        //PlayerRB = GetComponent<Rigidbody>();
        //PlayerRB.constraints = RigidbodyConstraints.FreezeAll;
        yield return new WaitForSeconds(0.2f);
        Scene scene = SceneManager.GetActiveScene();
        SceneManager.LoadScene(scene.name);
        //transform.position = PlayerStartPosition;
        //MoveUpAmount = LevelDataScript.Level1.UpMoves;
        //MoveDownAmount = LevelDataScript.Level1.DownMoves;
        //MoveLeftAmount = LevelDataScript.Level1.LeftMoves;
        //MoveRightAmount = LevelDataScript.Level1.RightMoves;
        //MUButton.interactable = true;
        //MDButton.interactable = true;
        //MLButton.interactable = true;
        //MRButton.interactable = true;
        //MULabel.text = MoveUpAmount.ToString();
        //MDLabel.text = MoveDownAmount.ToString();
        //MLLabel.text = MoveLeftAmount.ToString();
        //MRLabel.text = MoveRightAmount.ToString();
        //PlayerRB.constraints = RigidbodyConstraints.None;
        // RestartInProgress = false;
        // InMotion = false;
    }

    IEnumerator DelayInput()
    {
        yield return new WaitForSeconds(0.02f);
        RaycastHit hit;
        if (Physics.Raycast(transform.position, new Vector3(0,-1,0), out hit, Mathf.Infinity))
        {
            if (hit.transform.name == "CompleteLevel")
            {
                rb.velocity = new Vector3(0, -5, 0);
            }
            InMotion = false;
        }
        else
        {
            Debug.Log("Ran");
            rb.velocity = new Vector3(0, -5, 0);
        }
    }

    IEnumerator UnloadLevel()
    { 
        GameControllerScript.LevelsCompleted += 1;
        WinningSound.Play();
        yield return new WaitForSeconds(2.0f);
        ChangeScene("LevelSelect");
    }

    void ChangeScene(string SceneName)
    {
        SceneManager.LoadScene(SceneName, LoadSceneMode.Single);
    }

    IEnumerator InitiateRestarting()
    {
        yield return new WaitForSeconds(2.2f);
        if (GameWon == false)
        {
            Restart();
        }
    }
}
