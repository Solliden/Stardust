﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScrollingBackground : MonoBehaviour
{
    public float ScrollSpeed;
    Vector3 StartPos;
    GameController GameMaster;
    void Start()
    {
        StartPos = transform.position;
        GameMaster = GameObject.Find("GameMaster").GetComponent<GameController>();
        if (GameMaster.ScrollingBackground_x_float != 0)
        {
            transform.position = new Vector3(GameMaster.ScrollingBackground_x_float, transform.position.y, transform.position.z);
        }
    }
    

    void Update()
    {
        transform.Translate(Time.deltaTime * ScrollSpeed,0f,0f);
        GameMaster.ScrollingBackground_x_float = transform.position.x;
        if (transform.position.x >= 205) // Full length of Canvas 1920x1080
        {
            transform.position = StartPos;
        }
    }
}
