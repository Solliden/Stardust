﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelSelectLoadIn : MonoBehaviour {

    GameObject PauseScreen;

    public void LoadLevel(string SceneName)
    {
        SceneManager.LoadScene(SceneName, LoadSceneMode.Single);

        PauseScreen = GameObject.Find("PauseMenu");

        if (PauseScreen != null)
        {
            PauseScreen.GetComponent<Canvas>().enabled = false;
        }

        Time.timeScale = 1.0f;

    }

}
