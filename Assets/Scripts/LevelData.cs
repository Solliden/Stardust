﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelDataStructure
{
    public int UpMoves = 0;
    public int DownMoves = 0;
    public int LeftMoves = 0;
    public int RightMoves = 0;
    public int LeastMoves = 0;
    public int LeastMovesAndBonusStar = 0;

    public LevelDataStructure(int UP, int DOWN,int LEFT,int RIGHT,int LEASTMOVES, int LEASTMOVESANDBONUSSTAR)
    {
        UpMoves = UP;
        DownMoves = DOWN;
        LeftMoves = LEFT;
        RightMoves = RIGHT;
        LeastMoves = LEASTMOVES;
        LeastMovesAndBonusStar = LEASTMOVESANDBONUSSTAR;
    }

}

[System.Serializable]
public class GameData
{
    public int LevelsCompleted;
    public int TotalMoves;
    public int TotalMovesUp;
    public int TotalMovesDown;
    public int TotalMovesLeft;
    public int TotalMovesRight;
    public int TotalDeaths;
    public int TotalStars;
}

[System.Serializable]
public class Settings
{
    public bool Sound;
    public float Volume;

    public float Head_r;
    public float Head_g;
    public float Head_b;
    public float Head_a;

    public float Body_r;
    public float Body_g;
    public float Body_b;
    public float Body_a;

    public float Limbs_r;
    public float Limbs_g;
    public float Limbs_b;
    public float Limbs_a;
    public Color HeadColour
    {
        get
        {
            return new Color(Head_r, Head_g, Head_b, Head_a);
        }
        set
        {
            Head_r = value.r;
            Head_g = value.g;
            Head_b = value.b;
            Head_a = value.a;
        }
    }
    public Color BodyColour
    {
        get
        {
            return new Color(Body_r, Body_g, Body_b, Body_a);
        }
        set
        {
            Body_r = value.r;
            Body_g = value.g;
            Body_b = value.b;
            Body_a = value.a;
        }
    }
    public Color LimbsColour
    {
        get
        {
            return new Color(Limbs_r, Limbs_g, Limbs_b, Limbs_a);
        }
        set
        {
            Limbs_r = value.r;
            Limbs_g = value.g;
            Limbs_b = value.b;
            Limbs_a = value.a;
        }
    }
}

[System.Serializable]
public class LevelProgress
{
    public int TotalMoves;
    public int TotalDeaths;
    public int LeastMovesMade;
    public int TotalStars;
    public bool BonusStarCollected;
    public bool CorrectLeastMoves;
    public bool CorrectLeastMovesWithStar;
    public bool LevelCompleted;
}
