﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using System;
using TMPro;
using UnityEngine.SceneManagement;

[Serializable]
public class GameController : MonoBehaviour {

    public static GameController GameMaster;
    public int LevelsCompleted;
    public int TotalMoves;
    public int TotalMovesUp;
    public int TotalMovesDown;
    public int TotalMovesLeft;
    public int TotalMovesRight;
    public int TotalDeaths;
    public int TotalStars;
    public GameObject PauseScreen;
    AudioSource audioData;
    GameObject StatsCanvas;
    GameObject TextTotalMoves;
    GameObject TextMovesUp;
    GameObject TextMovesDown;
    GameObject TextMovesLeft;
    GameObject TextMovesRight;
    GameObject TextTotalDeaths;
    GameObject TextLevelsCompleted;
    public LevelDataStructure CurrentLevel;
    public int CurrentLevelInteger;
    string CurrentSceneName;
    public bool Sound;
    public float Volume;
    public Color HeadColour;
    public Color BodyColour;
    public Color LimbsColour;
    public float ScrollingBackground_x_float = 0f;
    public List<LevelProgress> LevelList = new List<LevelProgress>();
    public string TimeLeft;
    DateTime CurrentDate;
    DateTime SavedDate;
    TimeSpan TimeDifference;
    float TimePassed;
    float ExtraTime;
    public int Lives;
    int LifeGainTime = 300; // Time in seconds (e.g. 300 = every 5 minutes)
    public float ActualTimeLeft;


    void Awake()
    {
        if (GameMaster == null)
        {
            DontDestroyOnLoad(gameObject);
            GameMaster = this;
        }
        else if (GameMaster != this)
        {
            Destroy(gameObject);
        }
        LoadGameData();
        LoadSettings();
        for (int i = 0; i < 50; i++)
        {
            LevelProgress InitialiseClass = new LevelProgress();
            LevelList.Add(InitialiseClass);
        }
        LoadLevelInformation();
    }

    void Start()
    {
        CurrentDate = System.DateTime.Now;

        if (PlayerPrefs.HasKey("SavedDate") != false || PlayerPrefs.HasKey("SavedLives") != false)
        {
            Lives = PlayerPrefs.GetInt("SavedLives");
            ExtraTime = PlayerPrefs.GetFloat("ExtraTime");
            long DateAsInt = Convert.ToInt64(PlayerPrefs.GetString("SavedDate"));
            SavedDate = DateTime.FromBinary(DateAsInt);
            TimeDifference = CurrentDate.Subtract(SavedDate);
            CalculateLives();
        }
        else
        {
            Lives = 3;
        }
    }


    public void SaveGameData()
    {
        GameData Variables = new GameData();

        Variables.LevelsCompleted = LevelsCompleted;
        Variables.TotalMoves = TotalMoves;
        Variables.TotalMovesUp = TotalMovesUp;
        Variables.TotalMovesDown = TotalMovesDown;
        Variables.TotalMovesLeft = TotalMovesLeft;
        Variables.TotalMovesRight = TotalMovesRight;
        Variables.TotalDeaths = TotalDeaths;
        Variables.TotalStars = TotalStars;
        BinaryFormatter bf = new BinaryFormatter();
        FileStream file = File.Create(Application.persistentDataPath + "/GameData.gd");
        bf.Serialize(file,Variables);
        file.Close();
    }

    public void LoadGameData()
    {
        if (File.Exists(Application.persistentDataPath + "/GameData.gd"))
        {
            BinaryFormatter bf = new BinaryFormatter();
            FileStream file = File.Open(Application.persistentDataPath + "/GameData.gd", FileMode.Open);
            GameData Variables = (GameData)bf.Deserialize(file);
            file.Close();
            LevelsCompleted = Variables.LevelsCompleted;
            TotalMoves = Variables.TotalMoves;
            TotalMovesUp = Variables.TotalMovesUp;
            TotalMovesDown = Variables.TotalMovesDown;
            TotalMovesLeft = Variables.TotalMovesLeft;
            TotalMovesRight = Variables.TotalMovesRight;
            TotalDeaths = Variables.TotalDeaths;
            TotalStars = Variables.TotalStars;
            StatsCanvas = GameObject.Find("StatsPanel");
            if (StatsCanvas != null)
            {
                TextMovesLeft = GameObject.Find("MovesLeft");
                TextMovesLeft.GetComponent<TextMeshProUGUI>().text = "Moves Left: " + TotalMovesLeft;
                TextMovesRight = GameObject.Find("MovesRight");
                TextMovesRight.GetComponent<TextMeshProUGUI>().text = "Moves Right: " + TotalMovesRight;
                TextMovesUp = GameObject.Find("MovesUp");
                TextMovesUp.GetComponent<TextMeshProUGUI>().text = "Moves Up: " + TotalMovesUp;
                TextMovesDown = GameObject.Find("MovesDown");
                TextMovesDown.GetComponent<TextMeshProUGUI>().text = "Moves Down: " + TotalMovesDown;
                TextTotalMoves = GameObject.Find("TotalMoves");
                TextTotalMoves.GetComponent<TextMeshProUGUI>().text = "Total Moves: " + TotalMoves;
                TextTotalDeaths = GameObject.Find("TotalDeaths");
                TextTotalDeaths.GetComponent<TextMeshProUGUI>().text = "Total Deaths: " + TotalDeaths;
                TextLevelsCompleted = GameObject.Find("TotalHeartsCollected");
                TextLevelsCompleted.GetComponent<TextMeshProUGUI>().text = "Hearts Collected: " + LevelsCompleted;
            }
        }
    }

    public void LoadSettings()
    {
        if (File.Exists(Application.persistentDataPath + "/Settings.gd"))
        {
            BinaryFormatter bf = new BinaryFormatter();
            FileStream file = File.Open(Application.persistentDataPath + "/Settings.gd", FileMode.Open);
            Settings settings = (Settings)bf.Deserialize(file);
            file.Close();
            Sound = settings.Sound;
            Volume = settings.Volume;
            HeadColour = settings.HeadColour;
            BodyColour = settings.BodyColour;
            LimbsColour = settings.LimbsColour;
        }
        else // If file doesn't exist, set some default settings
        {
            Volume = 1.0f;
            // Force default character colours
            HeadColour = new Color32(204, 116, 0, 255);
            BodyColour = new Color32(255, 255, 255, 255);
            LimbsColour = new Color32(34, 36, 27, 255);
        }
    }

    public void SaveSettings()
    {
        Settings settings = new Settings();

        settings.Sound = Sound;
        settings.Volume = Volume;
        settings.HeadColour = HeadColour;
        settings.BodyColour = BodyColour;
        settings.LimbsColour = LimbsColour;
        BinaryFormatter bf = new BinaryFormatter();
        FileStream file = File.Create(Application.persistentDataPath + "/Settings.gd");
        bf.Serialize(file, settings);
        file.Close();

        LoadSettings(); // After saving the settings into a file, current settings need to be updated too
    }

    public void LoadLevelInformation()
    {
        if (File.Exists(Application.persistentDataPath + "/LevelProgress.gd"))
        {
            BinaryFormatter bf = new BinaryFormatter();
            FileStream file = File.Open(Application.persistentDataPath + "/LevelProgress.gd", FileMode.Open);
            List<LevelProgress> tempLevelList = (List<LevelProgress>)bf.Deserialize(file);
            file.Close();
            for (int i = 0; i < 50; i++)
            {
                LevelList[i].TotalMoves = tempLevelList[i].TotalMoves;
                LevelList[i].TotalDeaths = tempLevelList[i].TotalDeaths;
                LevelList[i].TotalStars = tempLevelList[i].TotalStars;
                LevelList[i].LeastMovesMade = tempLevelList[i].LeastMovesMade;
                LevelList[i].BonusStarCollected = tempLevelList[i].BonusStarCollected;
                LevelList[i].CorrectLeastMoves = tempLevelList[i].CorrectLeastMoves;
                LevelList[i].CorrectLeastMovesWithStar = tempLevelList[i].CorrectLeastMovesWithStar;
                LevelList[i].LevelCompleted = tempLevelList[i].LevelCompleted;
            }
        }
    }

    public void SaveLevelInformation()
    {
        BinaryFormatter bf = new BinaryFormatter();
        FileStream file = File.Create(Application.persistentDataPath + "/LevelProgress.gd");
        List<LevelProgress> tempLevelList = new List<LevelProgress>();

        for (int i = 0 ; i < 50 ; i++)
        {
            LevelProgress newLevel = new LevelProgress();
            newLevel.TotalMoves = LevelList[i].TotalMoves;
            newLevel.TotalDeaths = LevelList[i].TotalDeaths;
            newLevel.TotalStars = LevelList[i].TotalStars;
            newLevel.LeastMovesMade = LevelList[i].LeastMovesMade;
            newLevel.BonusStarCollected = LevelList[i].BonusStarCollected;
            newLevel.CorrectLeastMoves = LevelList[i].CorrectLeastMoves;
            newLevel.CorrectLeastMovesWithStar = LevelList[i].CorrectLeastMovesWithStar;
            newLevel.LevelCompleted = LevelList[i].LevelCompleted;
            tempLevelList.Add(newLevel);
        }
        bf.Serialize(file, tempLevelList);
        file.Close();

    }
    public void PlayButtonSound() // This is used by buttons to make a sound on click
    {
        audioData = GetComponent<AudioSource>();
        audioData.Play(0);
    }

    public void SetCurrentLevel(LevelDataStructure LevelName)
    {
        CurrentLevel = LevelName;
    }

    public void PauseGame()
    {
        if (CurrentSceneName.Contains("Level") && CurrentSceneName != "LevelSelect") //Only able to pause or unpause in levels
        {
            PauseScreen.GetComponent<Canvas>().enabled = true;
            Time.timeScale = 0.0f;
        }
    }

    public void UnpauseGame()
    {
        if (CurrentSceneName.Contains("Level") && CurrentSceneName != "LevelSelect") //Only able to pause or unpause in levels
        {
            PauseScreen.GetComponent<Canvas>().enabled = false;
            Time.timeScale = 1.0f;
        }
    }

    void Update()
    {
        if (Lives != 10)
        {
            TimePassed += Time.deltaTime;
            ActualTimeLeft = LifeGainTime - TimePassed;
            TimeLeft = ((int)(ActualTimeLeft / 60)).ToString() + ":" + ((int)(ActualTimeLeft % 60)).ToString("00");
        }
        if (TimePassed >= LifeGainTime) 
        {
            UpdateLives();
        }

        CurrentSceneName = SceneManager.GetActiveScene().name;

        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (CurrentSceneName == "Statistics" || CurrentSceneName == "LevelSelect" || CurrentSceneName == "Settings")
            {
                gameObject.GetComponent<LevelSelectLoadIn>().LoadLevel("MainMenu");
            }
            else if (PauseScreen.GetComponent<Canvas>().enabled == false)
            {
                PauseGame();
            }
            else if (PauseScreen.GetComponent<Canvas>().enabled == true)
            {
                UnpauseGame();
            }
        }
    }

    public void CalculateLives()
    {
        int tempLives = 0;
        float TotalMinutes = (float)TimeDifference.TotalMinutes + (ExtraTime / 60);
        if (TotalMinutes >= 5)
        {
            for (int i = 0; TotalMinutes >= 5; i++)
            {
                TotalMinutes -= 5;
                tempLives += 1;
            }
            if (Lives < 10)
            {
                if (Lives + tempLives >= 10)
                {
                    Lives = 10;
                    TotalMinutes = 0; // Set to 0 so that if Lives are at 10 already, once a new life is taken off, the counter starts from 5 minutes
                }
                else
                {
                    Lives += tempLives;
                }
            }
        }
            
        TimePassed += (TotalMinutes * 60); // Add any excess time to live counter ... Multiplication is to get seconds out of minutes (e.g. 3 * 60 = 180 seconds)

    }
    public void UpdateLives()
    {
        TimePassed -= LifeGainTime; // Reset time counter
        if (Lives < 10)
        {
            Lives += 1;
        }
    }
    public void DeductLives() // Call this when a new level is started or restarted
    {
        Lives -= 1;
    }
    void OnApplicationQuit()
    {
        PlayerPrefs.SetString("SavedDate", System.DateTime.Now.ToBinary().ToString());
        PlayerPrefs.SetInt("SavedLives", Lives);
        PlayerPrefs.SetFloat("ExtraTime", TimePassed);

        // Save Data when application is closed, for example, mid level, so that stats still save
        // Prevents resetting stats mid-run to get a better result
        SaveLevelInformation();
        SaveGameData();
    }

}

public class LevelData
{
    //UP DOWN LEFT RIGHT
    public LevelDataStructure Level1 = new LevelDataStructure(10, 10, 10, 10, 6, 6);
    public LevelDataStructure Level2 = new LevelDataStructure(5, 5, 5, 3, 13, 13);
    public LevelDataStructure Level3 = new LevelDataStructure(3, 3, 3, 3, 7, 7);
    public LevelDataStructure Level4 = new LevelDataStructure(4, 3, 11, 4, 21, 21);
}
