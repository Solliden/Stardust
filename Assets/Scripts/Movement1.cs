﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

[System.Serializable]
public class Movement1 : MonoBehaviour {

    Rigidbody rb;
    public bool MoveUp = false;
    public bool MoveDown = false;
    public bool MoveLeft = false;
    public bool MoveRight = false;
    bool PositionUpdated = false;
    Vector3 CurrentPosition;
    float Animation;
    enum Direction {STILL,NORTH,EAST,SOUTH,WEST};
    public int CurrentDirection = (int)Direction.STILL;
    public bool TargetSet = false;
    Vector3 Target;
    bool CurrentlyMoving = false;
    public int MoveUpAmount = 0;
    public int MoveDownAmount = 0;
    public int MoveLeftAmount = 0;
    public int MoveRightAmount = 0;
    LevelDataStructure CurrentLevel;
    GameObject GameMaster;
    GameController GameMasterScript;
    Text MULabel;
    Text MDLabel;
    Text MLLabel;
    Text MRLabel;
    Button MUButton;
    Button MDButton;
    Button MLButton;
    Button MRButton;
    bool GameWon = false;
    public AudioSource WinningSound;
    public AudioSource FallingSound;
    public bool RestartThroughButton;
    bool Jumping = false;
    bool TargetSetForJumping = false;
    bool AlreadyRestarting = false;

    void Start()
    {
        rb = GetComponent<Rigidbody>();
        GameMaster = GameObject.Find("GameMaster");
        GameMasterScript = GameMaster.GetComponent<GameController>();
        if (GameMaster != null)
        {
            CurrentLevel = GameMaster.GetComponent<GameController>().CurrentLevel;
        }
        //MULabel = GameObject.Find("MoveUpCounter").GetComponent<Text>();
        //MDLabel = GameObject.Find("MoveDownCounter").GetComponent<Text>();
        //MLLabel = GameObject.Find("MoveLeftCounter").GetComponent<Text>();
        //MRLabel = GameObject.Find("MoveRightCounter").GetComponent<Text>();
        //MRButton = GameObject.Find("MoveRight").GetComponent<Button>();
        //MDButton = GameObject.Find("MoveDown").GetComponent<Button>();
        //MUButton = GameObject.Find("MoveUp").GetComponent<Button>();
        //MLButton = GameObject.Find("MoveLeft").GetComponent<Button>();
        MoveUpAmount = CurrentLevel.UpMoves;
        MoveDownAmount = CurrentLevel.DownMoves;
        MoveLeftAmount = CurrentLevel.LeftMoves;
        MoveRightAmount = CurrentLevel.RightMoves;
        //MULabel.text = MoveUpAmount.ToString();
        //MDLabel.text = MoveDownAmount.ToString();
        //MLLabel.text = MoveLeftAmount.ToString();
        //MRLabel.text = MoveRightAmount.ToString();
    }


    void Update()
    {
        if (transform.position.y <= -2 && GameWon == false && AlreadyRestarting == false)
        {
            Restart();
        }


        if (Input.GetKeyDown(KeyCode.UpArrow) && CurrentlyMoving == false && MoveUpAmount > 0 || Input.GetKeyDown(KeyCode.W) && CurrentlyMoving == false && MoveUpAmount > 0)
        {
            MoveUp = true;
        }
        if (Input.GetKeyDown(KeyCode.RightArrow) && CurrentlyMoving == false && MoveRightAmount > 0 || Input.GetKeyDown(KeyCode.D) && CurrentlyMoving == false && MoveRightAmount > 0)
        {
            MoveRight = true;
        }
        if (Input.GetKeyDown(KeyCode.DownArrow) && CurrentlyMoving == false && MoveDownAmount > 0 || Input.GetKeyDown(KeyCode.S) && CurrentlyMoving == false && MoveDownAmount > 0)
        {
            MoveDown = true;
        }
        if (Input.GetKeyDown(KeyCode.LeftArrow) && CurrentlyMoving == false && MoveLeftAmount > 0 || Input.GetKeyDown(KeyCode.A) && CurrentlyMoving == false && MoveLeftAmount > 0) 
        {
            MoveLeft = true;
        }

        CheckDirection();
       //Debug.Log("Jumping: " + Jumping + " " + "TargetSet: " + TargetSet + " " + "TargetSetForJumping: " + TargetSetForJumping);
        if (Jumping == true & TargetSet == true && TargetSetForJumping == true)
        {
            Jump();
            Debug.Log(Target);
        }
        else
        {
            Move();
        }
    }


    public void CheckDirection()
    {
        if (TargetSet == false)
        {
            if (MoveUp == true)
            {
                MoveUpAmount -= 1;
                //MULabel.text = MoveUpAmount.ToString();
                if (MoveUpAmount <= 0)
                {
                    //MUButton.interactable = false;
                }
                CurrentDirection = (int)Direction.NORTH;
                CreateTarget();
            }
            else if (MoveDown == true)
            {
                MoveDownAmount -= 1;
                //MDLabel.text = MoveDownAmount.ToString();
                if (MoveDownAmount <= 0)
                {
                    //MDButton.interactable = false;
                }
                CurrentDirection = (int)Direction.SOUTH;
                CreateTarget();
            }
            else if (MoveLeft == true)
            {
                MoveLeftAmount -= 1;
                //MLLabel.text = MoveLeftAmount.ToString();
                if (MoveLeftAmount <= 0)
                {
                    //MLButton.interactable = false;
                }
                CurrentDirection = (int)Direction.WEST;
                CreateTarget();
            }
            else if (MoveRight == true)
            {
                MoveRightAmount -= 1;
                //MRLabel.text = MoveRightAmount.ToString();
                if (MoveRightAmount <= 0)
                {
                    //MRButton.interactable = false;
                }
                CurrentDirection = (int)Direction.EAST;
                CreateTarget();
            }
        }
    }

    public void CreateTarget()
    {
        GameMaster.GetComponent<GameController>().PlayButtonSound();
        switch (CurrentDirection)
        {
            case (int)Direction.NORTH:
                if (Jumping == true)
                {
                    Target = new Vector3(transform.position.x - 2.0f, transform.position.y, transform.position.z);
                    TargetSetForJumping = true;
                }
                else
                {
                    Target = new Vector3(transform.position.x - 1.0f, transform.position.y, transform.position.z);
                }
                GameMasterScript.TotalMovesUp += 1;
                break;
            case (int)Direction.EAST:
                if (Jumping == true)
                {
                    Target = new Vector3(transform.position.x, transform.position.y, transform.position.z + 2.0f);
                    TargetSetForJumping = true;
                }
                else
                {
                    Target = new Vector3(transform.position.x, transform.position.y, transform.position.z + 1.0f);
                }
                GameMasterScript.TotalMovesRight += 1;
                break;
            case (int)Direction.SOUTH:
                if (Jumping == true)
                {
                    Target = new Vector3(transform.position.x + 2.0f, transform.position.y, transform.position.z);
                    TargetSetForJumping = true;
                }
                else
                {
                    Target = new Vector3(transform.position.x + 1.0f, transform.position.y, transform.position.z);
                }
                GameMasterScript.TotalMovesDown += 1;
                break;
            case (int)Direction.WEST:
                if (Jumping == true)
                {
                    Target = new Vector3(transform.position.x, transform.position.y, transform.position.z - 2.0f);
                    TargetSetForJumping = true;
                }
                else
                {
                    Target = new Vector3(transform.position.x, transform.position.y, transform.position.z - 1.0f);
                }
                GameMasterScript.TotalMovesLeft += 1;
                break;
        }
        GameMasterScript.TotalMoves += 1;
        //GameMasterScript.Save();
        TargetSet = true;
        UpdatePosition();
    }

    public void Move()
    {
        if (TargetSet == true && transform.position != Target)
        {
            transform.position = Vector3.MoveTowards(transform.position, Target, Time.deltaTime * 4);
        }
        else if (TargetSet == true && transform.position == Target)
        {
            EndMove();
        }

    }

    public void Jump()
    {
        if (TargetSet == true && transform.position != Target && Jumping == true && transform.position.x > Target.x + 0.1f && CurrentDirection == (int)Direction.NORTH || TargetSet == true && transform.position != Target && Jumping == true && transform.position.x < Target.x - 0.1f && CurrentDirection == (int)Direction.SOUTH || TargetSet == true && transform.position != Target && Jumping == true && transform.position.z > Target.z + 0.1f && CurrentDirection == (int)Direction.WEST || TargetSet == true && transform.position != Target && Jumping == true && transform.position.z < Target.z - 0.1f && CurrentDirection == (int)Direction.EAST)
        {
            Animation += Time.deltaTime;

            Animation = Animation % 5;

            transform.position = MathParabola.Parabola(CurrentPosition, Target, 1.0f, Animation / 0.7f);
        }
        else if (TargetSet == true && Jumping == true && transform.position != Target)
        {
            transform.position = Vector3.MoveTowards(transform.position, Target, Time.deltaTime * 4);
        }
        else if (TargetSet == true && transform.position == Target)
        {
            EndMove();
        }
    }

    public void EndMove()
    {
        RaycastHit hit;
        if (!Physics.Raycast(transform.position, new Vector3(0, -1, 0), out hit, Mathf.Infinity))
        { 
            rb.velocity = new Vector3(0, -5, 0);
            CurrentlyMoving = true;
        }
        ResetAfterMove();
    }


    public void UpdatePosition()
    {
        if (PositionUpdated == false)
        {
            CurrentPosition = transform.position;
            Animation = 0.0f;
            PositionUpdated = true;
        }
    }

    public void ResetAfterMove()
    {
        TargetSetForJumping = false;
        PositionUpdated = false;
        Animation = 0.0f;
        MoveLeft = MoveRight = MoveUp = MoveDown = TargetSet = false;
    }

    void RunOutOfMoves()
    {
        if (MoveUpAmount <= 0 && MoveDownAmount <= 0 && MoveLeftAmount <= 0 && MoveRightAmount <= 0)
        {
            StartCoroutine(InitiateRestarting());
        }
    }

    IEnumerator InitiateRestarting()
    {
        yield return new WaitForSeconds(2.2f);
        if (GameWon == false)
        {
            Restart();
        }
    }

    public void Restart()
    {
        AlreadyRestarting = true;
        GameMasterScript.TotalDeaths += 1;
        //GameMasterScript.Save();
        if (RestartThroughButton == false)
        {
            if (FallingSound.isPlaying == false)
            {
                FallingSound.Play();
            }
        }
        StartCoroutine(RestartDelay());

    }

    IEnumerator RestartDelay()
    {
        yield return new WaitForSeconds(0.2f);
        Scene CurrentScene = SceneManager.GetActiveScene();
        ChangeScene(CurrentScene.name);
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.name == "JumpBlock")
        {
            Jumping = true;
        }
        else if (other.name != "JumpBlock")
        {
            Jumping = false;
        }
        if (other.name == "WinObject")
        {
            Destroy(other.gameObject);
            GameWon = true;
            CurrentlyMoving = true;
            StartCoroutine(LevelCompleted());
        }
    }
    IEnumerator LevelCompleted()
    {
        GameMasterScript.LevelsCompleted += 1;
        WinningSound.Play();
        yield return new WaitForSeconds(2.0f);
        ChangeScene("LevelSelect");
    }

    void ChangeScene(string SceneName)
    {
        SceneManager.LoadScene(SceneName, LoadSceneMode.Single);
    }
}
