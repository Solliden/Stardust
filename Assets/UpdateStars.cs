﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UpdateStars : MonoBehaviour
{
    public Text StarsCollected;
    GameController GameMaster;
    void Start()
    {
        GameMaster = GameObject.Find("GameMaster").GetComponent<GameController>();
        StarsCollected.text = GameMaster.TotalStars.ToString() + " / 150";
    }
}
