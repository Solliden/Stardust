﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CallRestart : MonoBehaviour
{
    Motion PlayerScript;
    void Start()
    {
        PlayerScript = GameObject.Find("Player").GetComponent<Motion>();
    }

    public void RestartLevel()
    {
        StartCoroutine(PlayerScript.RestartDelay());
    }
}
