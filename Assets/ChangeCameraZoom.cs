﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChangeCameraZoom : MonoBehaviour
{
    GameObject MainCamera;

    private void Start()
    {
        MainCamera = GameObject.Find("Main Camera");
    }
    public void CallZoomChange()
    {
        MainCamera.GetComponent<FollowScript>().ChangeZoom();
    }
}
