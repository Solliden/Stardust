﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameSettings : MonoBehaviour
{
    bool Sound;
    GameController GameMaster;
    Sprite SoundButton_sprite;
    public Sprite On;
    public Sprite Off;

    public FlexibleColorPicker ColourPicker;
    Color32 Color;
    public Color Head;
    public Color Body;
    public Color Limbs;
    public GameObject Head_Toggle;
    public GameObject Body_Toggle;
    public GameObject Limbs_Toggle;
    public GameObject Player;
    public Slider VolumeSlider;
    public Text VolumePercentage;
    Material[] PlayerMaterials;
    void Start()
    {
        //Adds a listener to the main slider and invokes a method when the value changes.
        VolumeSlider.onValueChanged.AddListener(delegate { UpdateVolume(); }); // Googled for a listener when value changes rather than always updating
        GameMaster = GameObject.Find("GameMaster").GetComponent<GameController>();
        PlayerMaterials = Player.GetComponent<Renderer>().materials; // 0 is limbs , 1 is body, 2 is head
        UseCurrentSettings();
    }
    public void UpdateVolume()
    {
        GameMaster.Volume = VolumeSlider.value;
        VolumePercentage.text = Mathf.Round((VolumeSlider.value * 100)) + "%";
    }
    void UseCurrentSettings()
    {
        VolumePercentage.text = Mathf.Round((VolumeSlider.value * 100)) + "%";
        Sound = GameMaster.Sound;
        if (Sound == false)
        {
            VolumeSlider.interactable = false;
        }
        else
        {
            VolumeSlider.interactable = true;
        }

        VolumeSlider.value = GameMaster.Volume;

        Head = GameMaster.HeadColour;
        Body = GameMaster.BodyColour;
        Limbs = GameMaster.LimbsColour;

        ColourPicker.startingColor = Head;
        ColourPicker.color = ColourPicker.startingColor; //Use this to refresh ColourPicker UI on screen

        PlayerMaterials[0].color = Limbs;
        PlayerMaterials[1].color = Body;
        PlayerMaterials[2].color = Head;
    }

    public void SaveSettings()
    {
        GameMaster.Sound = Sound;
        GameMaster.Volume = VolumeSlider.value;
        GameMaster.HeadColour = Head;
        GameMaster.BodyColour = Body;
        GameMaster.LimbsColour = Limbs;
        GameMaster.SaveSettings();
    }

    public void UpdateHeadColour()
    {
        if (Head_Toggle.GetComponent<Toggle>().isOn == false)
        {
            Head = ColourPicker.color;
        }
        else
        {
            ColourPicker.color = Head;
        }
    }

    public void UpdateBodyColour()
    {
        if (Body_Toggle.GetComponent<Toggle>().isOn == false)
        {
            Body = ColourPicker.color;
        }
        else
        {
            ColourPicker.color = Body;
        }
    }
    public void UpdateLimbsColour()
    {
        if (Limbs_Toggle.GetComponent<Toggle>().isOn == false)
        {
            Limbs = ColourPicker.color;
        }
        else
        {
            ColourPicker.color = Limbs;
        }
    }

    public void UpdateToDefaultColour()
    {
        Head = new Color32(204, 116, 0, 255);
        Body = new Color32(255, 255, 255, 255);
        Limbs = new Color32(34, 36, 27, 255);
        PlayerMaterials[2].color = Head;
        PlayerMaterials[1].color = Body;
        PlayerMaterials[0].color = Limbs;
        if (Head_Toggle.GetComponent<Toggle>().isOn == true)
        {
            ColourPicker.color = Head;
        }
        else if (Body_Toggle.GetComponent<Toggle>().isOn == true)
        {
            ColourPicker.color = Body;
        }
        else if (Limbs_Toggle.GetComponent<Toggle>().isOn == true)
        {
            ColourPicker.color = Limbs;
        }
    }

    private void Update()
    {
        if (Head_Toggle.GetComponent<Toggle>().isOn == true)
        {
            Head = ColourPicker.color;
            PlayerMaterials[2].color = Head;
        }
        else if (Body_Toggle.GetComponent<Toggle>().isOn == true)
        {
            Body = ColourPicker.color;
            PlayerMaterials[1].color = Body;
        }
        else if (Limbs_Toggle.GetComponent<Toggle>().isOn == true)
        {
            Limbs = ColourPicker.color;
            PlayerMaterials[0].color = Limbs;
        }
    }
}
