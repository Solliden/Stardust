﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ChangeColourScript : MonoBehaviour
{
    public FlexibleColorPicker fcp;
    public GameObject cube;
    Color32 Color;

    private void Start()
    {
        fcp.color = new Color32(8, 94, 80, 255);
        Color = fcp.color;
        Debug.Log(Color);
    }
    private void Update()
    {
        Color = fcp.color;
        Debug.Log(Color);
        cube.GetComponent<Renderer>().material.color = fcp.color;
    }
}
