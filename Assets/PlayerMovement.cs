﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerMovement : MonoBehaviour
{

    bool Falling = false;
    bool Restarting = false;
    bool BlockTypeChecked = false;
    bool Moving = false;
    private Animator anim;
    private float startTime;
    float distCovered;
    float journeyLength;
    public float speed = 1.0F;  //Player movement speed (Units per second)
    Vector3 StartPosition;
    Vector3 EndPosition;
    Vector3 ObjectPosition;
    enum Direction { UP, DOWN, LEFT, RIGHT };
    int CurrentDirection;
    bool NextMoveSet = false;
    GameObject ObjectHit;
    bool FinalTargetSet = false;

    int MoveUpAmount = 5;
    int MoveDownAmount = 5;
    int MoveLeftAmount = 5;
    int MoveRightAmount= 5;

    void Start()
    {
        anim = gameObject.GetComponentInChildren<Animator>();
    }

    void Update()
    {
        CheckFalling(); //Check if the player should be falling

        Fall(); //If the player should be falling, make him fall

        SetTarget(); //Check where player clicked, make that final destination

        CheckNextMoveType(); //Check what kind of movement is needed for next move(Regular move,jump, etc.)

        Move(); //Move player by one unit

        Jump(); //Jump the player by two units

    }

    void CheckFalling()
    {
        RaycastHit CheckFloor;
        if (!Physics.Raycast(transform.position, new Vector3(0, -5, 0), out CheckFloor, Mathf.Infinity) && Falling == false) {

            Falling = true;
            Debug.Log("Player is falling");
        }
    }

    void Fall()
    {
        if (Falling == true)
        {
            transform.Translate(new Vector3(0,-3,0) * Time.deltaTime);
            if (transform.position.y <= -3 && Restarting == false)
            {
                Restarting = true;
                StartCoroutine(RestartDelay());
                Debug.Log("Restarting the level");
            }
        }
    }

    void CheckNextMoveType()
    {
        if (BlockTypeChecked == false)
        {
            RaycastHit CheckFloor;
            if (Physics.Raycast(transform.position, new Vector3(0, -1, 0), out CheckFloor, Mathf.Infinity))
            {
                if (NextMoveSet == true)
                {
                    if (DeductMove(CurrentDirection) == true)
                    {
                        //Check for object name, and based on that enable type of movement (Move = true, Jump = true, etc.)
                        if (CheckFloor.transform.gameObject.name.Contains("LevelBlock") == true)
                        {
                            BlockTypeChecked = true;
                            Moving = true;
                            StartPosition = transform.position;
                            journeyLength = Vector3.Distance(StartPosition, EndPosition);
                            startTime = Time.time;
                            distCovered = 0;
                            Debug.Log("Player starting movement");
                        }
                    }
                }
            }
        }
    }

    void Move()
    {
        if (Moving == true)
        {
            if (distCovered < journeyLength)
            {
                distCovered = (Time.time - startTime) * speed;
                float fractionOfJourney = distCovered / journeyLength;
                transform.position = Vector3.Lerp(StartPosition, EndPosition, fractionOfJourney);
                anim.SetInteger("AnimationPar", 1);
            }
            else
            {
                Moving = false;
                TargetReached();
                Debug.Log("Player stopping movement");
            }
        }
    }

    void Jump()
    {

    }

    void SetTarget()
    {
        if (Input.GetMouseButtonDown(0) && FinalTargetSet == false)
        {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;
            if (Physics.Raycast(ray, out hit, Mathf.Infinity))
            {
                if (hit.transform.gameObject.name.Contains("Block") == true)
                {
                    ObjectPosition = hit.transform.position + new Vector3(0, 0.55f, 0); // Adds 0.55f to adjust for player being 0.55f above 0 point in world space
                    ObjectHit = hit.transform.gameObject;
                    if (SetDirection() == true)
                    {
                        FinalTargetSet = true;
                        Debug.Log("Target to move to clicked");
                        transform.LookAt(ObjectPosition);
                    }
                }
            }
        }
        if (NextMoveSet == false && FinalTargetSet == true)
            {
                Debug.Log("Setting Direction, Moves enough");
                if ((Direction)CurrentDirection == Direction.UP)
                {
                    EndPosition = new Vector3(transform.position.x - 1, transform.position.y, transform.position.z);
                    NextMoveSet = true;
                }
                else if ((Direction)CurrentDirection == Direction.DOWN)
                {
                    EndPosition = new Vector3(transform.position.x + 1, transform.position.y, transform.position.z);
                    NextMoveSet = true;
                }
                else if ((Direction)CurrentDirection == Direction.LEFT)
                {
                    EndPosition = new Vector3(transform.position.x, transform.position.y, transform.position.z - 1);
                    NextMoveSet = true;
                }
                else if ((Direction)CurrentDirection == Direction.RIGHT)
                {
                    EndPosition = new Vector3(transform.position.x, transform.position.y, transform.position.z + 1);
                    NextMoveSet = true;
                }
            }
    }


    bool SetDirection()
    {
        if (CheckAxis() == true)
        {
            Debug.Log("Axis is OK, checking enough moves");
            if (transform.position.x > ObjectPosition.x)
            {
                CurrentDirection = (int)Direction.UP;
                return CheckEnoughMoves(CurrentDirection);
            }
            else if (transform.position.x < ObjectPosition.x)
            {
                CurrentDirection = (int)Direction.DOWN;
                return CheckEnoughMoves(CurrentDirection);
            }
            else if (transform.position.z > ObjectPosition.z)
            {
                CurrentDirection = (int)Direction.LEFT;
                return CheckEnoughMoves(CurrentDirection);
            }
            else if (transform.position.z < ObjectPosition.z)
            {
                CurrentDirection = (int)Direction.RIGHT;
                return CheckEnoughMoves(CurrentDirection);
            }
        }
        Debug.Log("Axis is not OK, Player won't move");
        return false;
    }

    bool CheckAxis() {

        if (transform.position.x != ObjectHit.transform.gameObject.transform.position.x && transform.position.z != ObjectHit.transform.position.z)
        {
            return false;
        }
        else {
            return true;
        }
    }

    bool CheckEnoughMoves(int CurrentDirection)
    {
        if (CurrentDirection == (int)Direction.UP && MoveUpAmount > 0)
        {
            return true;
        }
        else if (CurrentDirection == (int)Direction.DOWN && MoveDownAmount > 0)
        {
            return true;
        }
        else if (CurrentDirection == (int)Direction.LEFT && MoveLeftAmount > 0)
        {
            return true;
        }
        else if (CurrentDirection == (int)Direction.RIGHT && MoveRightAmount > 0)
        {
            return true;
        }
        Debug.Log("Not enough moves, Player won't move");
        return false;
    }

    bool DeductMove(int CurrentDirection)
    {
        if (CurrentDirection == (int)Direction.UP)
        {
            MoveUpAmount -= 1;
            return true;
        }
        else if (CurrentDirection == (int)Direction.DOWN)
        {
            MoveDownAmount -= 1;
            return true;
        }
        else if (CurrentDirection == (int)Direction.LEFT)
        {
            MoveLeftAmount -= 1;
            return true;
        }
        else if (CurrentDirection == (int)Direction.RIGHT)
        {
            MoveRightAmount -= 1;
            return true;
        }
        return false;
    }

    void TargetReached()
    {
        if (Vector3.Distance(transform.position, ObjectPosition) > 0 && CheckEnoughMoves(CurrentDirection) == true)
        {
            BlockTypeChecked = false;
            NextMoveSet = false;
        }
        else
        {
            BlockTypeChecked = false;
            NextMoveSet = false;
            FinalTargetSet = false;
            anim.SetInteger("AnimationPar", 0);
        }
    }
    public IEnumerator RestartDelay()
    {
        yield return new WaitForSeconds(1.0f);
        Scene CurrentScene = SceneManager.GetActiveScene();
        ChangeScene(CurrentScene.name);
    }

    void ChangeScene(string SceneName)
    {
        SceneManager.LoadScene(SceneName, LoadSceneMode.Single);
    }
    IEnumerator LevelCompleted()
    {
        //WinningSound.Play(); Add sound for later to play when winning
        yield return new WaitForSeconds(2.0f);
        ChangeScene("LevelSelect");
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.name == "WinObject")
        {
            Destroy(GameObject.Find("WinObject"));
            StartCoroutine(LevelCompleted());
        }
    }
}
