﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowPlayer : MonoBehaviour
{
    Vector3 LightOffset;
    Vector3 PlayerPosition;
    GameObject player;
    float CameraSpeed = 2.0f;

    void Start()
    {
        player = GameObject.Find("Player");
        LightOffset = transform.position;
    }

    void Update()
    {
        PlayerPosition = player.transform.transform.position;
        transform.position = Vector3.Slerp(transform.position, LightOffset + PlayerPosition, Time.deltaTime * CameraSpeed);
    }
}
