﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Linq;

public class ExtraMoves : MonoBehaviour
{
    public int BonusMoves;
    void Start()
    {
        BonusMoves = Int32.Parse(transform.name.Substring(transform.name.Length - 2)); //Prefab names are e.g. BonusMove_05
    }
}
