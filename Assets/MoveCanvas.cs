﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MoveCanvas : MonoBehaviour
{
    float SlideSpeed = 8.0f;
    private float startTime;
    private float journeyLength;
    bool LoadedIn = false;
    Vector3 SlidePosition;

    private void Awake()
    {
        transform.position = new Vector3(-175.0f, transform.position.y, transform.position.z); //Move UI panel from screen
        LoadedIn = true;
        SlidePosition = new Vector3(0f, transform.position.y, transform.position.z);
        journeyLength = Vector3.Distance(transform.position, SlidePosition);
    }
    void Start()
    {
        startTime = Time.time;
    }
    void Update()
    {
        float distCovered = (Time.time - startTime) * SlideSpeed;
        float fractionOfJourney = distCovered / journeyLength;
        transform.position = Vector3.Lerp(transform.position, SlidePosition, fractionOfJourney);
    }

    public void SlideOut(string LevelName)
    {
        LoadedIn = false;
        SlidePosition = new Vector3(185.0f, transform.position.y, transform.position.z);
        startTime = Time.time;
        StartCoroutine(SlidePanelOut(LevelName));
    }


    IEnumerator SlidePanelOut(string LevelName)
    {
        yield return new WaitForSeconds(0.6f);
        ChangeScene(LevelName);
    }
    
    void ChangeScene(string LevelName)
    {
        LevelSelectLoadIn LoadLevels = gameObject.AddComponent<LevelSelectLoadIn>();
        LoadLevels.LoadLevel(LevelName);
    }
}
