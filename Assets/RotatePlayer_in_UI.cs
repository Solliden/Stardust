﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotatePlayer_in_UI : MonoBehaviour
{
    public float RotationSpeed;
    void Update()
    {
        transform.Rotate(0, RotationSpeed * Time.deltaTime, 0);
    }
}
