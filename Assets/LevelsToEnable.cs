﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LevelsToEnable : MonoBehaviour
{
    int HighestLevel;
    GameController GameMaster;
    void Start()
    {
        GameMaster = GameObject.Find("GameMaster").GetComponent<GameController>();
        HighestLevel = GameMaster.LevelsCompleted + 1;
        for (int i = 2; i < 51; i++) //Check from "BtnLevel2" to "BtnLevel50" assuming there is 50 levels in total
        {
            if (HighestLevel >= i)
            {
                GameObject.Find("BtnLevel" + i).GetComponent<Button>().interactable = true;
            }
            else
            {
                GameObject.Find("BtnLevel" + i).GetComponent<Button>().interactable = false;
            }
        }
    }
}
