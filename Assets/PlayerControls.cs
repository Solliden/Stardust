﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerControls : MonoBehaviour
{
    GameObject PlayerController;

    void Start()
    {
        PlayerController = GameObject.Find("PlayerController");
    }

    void OnTriggerEnter(Collider other)
    {
        Debug.Log(other.name);
        PlayerController.GetComponent<PlayerController>().ObjectTouched(other.name);
    }
}
