﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System;

public class Motion : MonoBehaviour
{

    private Animator anim;

    bool PauseCharacter = false;
    bool KeepRunning = false;
    bool KeepJumping = false;
    bool Teleporting = false;
    Vector3 Target;
    Vector3 NextBlock;
    public float RunningSpeed = 1.50f;
    public float SlowSpeed = 0.75f;
    public float JumpingSpeed = 1.25f;
    int MovesUp;
    int MovesDown;
    int MovesLeft;
    int MovesRight;
    int DestroyableBlockCount = 5;
    float TotalDistance = 0;
    bool FinalTargetSet = false;
    public bool BlocksToFall = false;
    enum Direction { UP, DOWN, LEFT, RIGHT };
    enum State {STANDING, RUNNING, JUMPING, FALLING, WINNING};
    int CurrentState = 0;
    int PreviousState = 0;
    int CurrentDirection;
    RaycastHit CheckFloor;
    float hopHeight = 1.0f;
    private bool hopping = false;
    public bool Falling = false;
    bool Restarting = false;
    string WalkingOnObject = "";
    public LevelDataStructure LevelMoves;
    GameController GameMaster;
    int fallAngle = 0;
    UpdatingLabels UpdateLabels = new UpdatingLabels(); //Script used to update moves on labels
    GameObject PauseMenu; // GameObject used to detect if pause menu is currently enabled
    Material[] PlayerMaterials; // Array to hold player's materials
    GameObject MaterialHolder;
    MovesTracker MoveTracker;
    bool LeastMovesMade = false;
    bool LevelFinished = false;
    bool MovesSwapped = false;
    public bool BonusMovesActivated = false;
    public int BonusMoves;
    List<GameObject> DisableMoveButtons = new List<GameObject>();
    GameObject BonusButton;
    GameObject LevelCamera;
    void Start()
    {
        anim = gameObject.GetComponentInChildren<Animator>();
        GameMaster = GameObject.Find("GameMaster").GetComponent<GameController>();
        LevelMoves = GameMaster.CurrentLevel;
        MovesUp = LevelMoves.UpMoves;
        MovesDown = LevelMoves.DownMoves;
        MovesLeft = LevelMoves.LeftMoves;
        MovesRight = LevelMoves.RightMoves;
        UpdateLabels.Start(); // Initialise hooks on labels present in scene
        PauseMenu = GameObject.Find("PauseMenu");
        MaterialHolder = GameObject.Find("Materials");
        PlayerMaterials = MaterialHolder.GetComponent<Renderer>().materials; // 0 is limbs , 1 is body, 2 is head
        PlayerMaterials[2].color = GameMaster.HeadColour;
        PlayerMaterials[1].color = GameMaster.BodyColour;
        PlayerMaterials[0].color = GameMaster.LimbsColour;
        MoveTracker = gameObject.GetComponent<MovesTracker>(); //Any references to this means updating player statistics
        BonusButton = GameObject.Find("BonusMoves");
        BonusButton.GetComponent<Button>().interactable = false; //By default it is set to interactable = false when there are no bonus moves available
        DisableMoveButtons.Add(GameObject.Find("MovesUp"));
        DisableMoveButtons.Add(GameObject.Find("MovesDown"));
        DisableMoveButtons.Add(GameObject.Find("MovesLeft"));
        DisableMoveButtons.Add(GameObject.Find("MovesRight"));
        LevelCamera = GameObject.Find("LevelCamera");
    }

    void Update()
    {
        if (PauseMenu.GetComponent<Canvas>().enabled && PauseCharacter == false)
        {
            PauseCharacter = true;
        }

        if (!PauseCharacter)
        {
            CheckFalling();
            if (!Fall())
            {

                if (Input.GetMouseButtonDown(0) == true && FinalTargetSet == false)
                {
                    CheckDirection();
                }
                if (FinalTargetSet == true && KeepRunning == false && KeepJumping == false && Teleporting == false)
                {
                    if (TotalDistance >= 1)
                    {
                        CheckObjectType();
                    }
                    else
                    {
                        RaycastHit CheckFloor;
                        if (Physics.Raycast(transform.position, new Vector3(0, -1, 0), out CheckFloor, Mathf.Infinity))
                        {
                            if (CheckFloor.transform.gameObject.name.Contains("TeleportBlock") == true)
                            {
                                int TeleportNumber = int.Parse(CheckFloor.transform.gameObject.name.Substring(CheckFloor.transform.gameObject.name.Length - 1));
                                if (TeleportNumber % 2 == 0)
                                {
                                    CheckObjectType();
                                }
                            }
                            else if (CheckFloor.transform.gameObject.name.Contains("RotationBlock") == true)
                            {
                                    UpdateTotalMoves();
                            }
                        }
                        FinalTargetSet = false;
                        TotalDistance = 0;
                        ChangeStates((int)State.STANDING);
                    }
                }
                if (KeepRunning == true)
                {
                    ChangeStates((int)State.RUNNING);
                    Run();
                }
                if (KeepJumping == true)
                {
                    ChangeStates((int)State.JUMPING);
                    StartCoroutine(Hop(NextBlock, JumpingSpeed));
                }
            }
        }
        else // Reasons for why the character is paused and how to unpause should be in this else statement
        {
            if (LevelFinished == true)
            {
                return;
            }
            RaycastHit ObjectInFront;
            // If enemy is no longer in vision, allow to move
            if (CurrentDirection == (int)Direction.RIGHT)
            {
                if (!Physics.Raycast(transform.position, new Vector3(1, 0, 0), out ObjectInFront, Mathf.Infinity))
                {
                    PauseCharacter = false;
                    ResumeState();
                }
            }
            else if (CurrentDirection == (int)Direction.LEFT)
            {
                if (!Physics.Raycast(transform.position, new Vector3(-1, 0, 0), out ObjectInFront, Mathf.Infinity))
                {
                    PauseCharacter = false;
                    ResumeState();
                }
            }
            else if (PauseMenu.GetComponent<Canvas>().enabled == false)
            {
                PauseCharacter = false;
            }
        }
    }

    void Run()
    {
        float step;
        if (WalkingOnObject == "SlowBlock")
        {
            step = SlowSpeed * Time.deltaTime; // calculate distance to move per step
        }
        else {
            step = RunningSpeed * Time.deltaTime; // calculate distance to move per step
        }
        transform.position = Vector3.MoveTowards(transform.position, NextBlock, step);
        if (transform.position == NextBlock)
        {
            KeepRunning = false;

            if (CheckFloor.transform != null && CheckFloor.transform.gameObject.name.Contains("LevelBlock") == true && BlocksToFall == true)
            {
                StartCoroutine(MakeObjectFall(CheckFloor.transform.gameObject));
            }
        }
    }

    void CheckDirection()
    {
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;
        if (Physics.Raycast(ray, out hit, Mathf.Infinity))
        {
            GameObject HitObject = hit.transform.gameObject;
            if (HitObject.name.Contains("Destroyable") == true)
            {
                if (DestroyableBlockCount >= 1)
                {
                    DestroyableBlockCount -= 1;
                    HitObject.GetComponent<DestroyEffect>().PlayExplosion(); //DestroyEffect script function
                }
            }
            else if (CheckAxis(HitObject) == true)
            {
                SetDirection(HitObject);

                if (FinalTargetSet == false)
                {
                    Target = new Vector3(HitObject.transform.position.x, HitObject.transform.position.y + 0.55f, HitObject.transform.position.z);
                    TotalDistance = Vector3.Distance(Target, transform.position);
                    FinalTargetSet = true;
                }

            }
            else
            {
                Debug.Log("Wrong Axis");
            }
        }
    }

    void CheckObjectType()
    {
        if (Physics.Raycast(transform.position, new Vector3(0, -1, 0), out CheckFloor, Mathf.Infinity))
        {
            WalkingOnObject = CheckFloor.transform.gameObject.name;
            if (CheckFloor.transform.gameObject.name.Contains("LevelBlock") == true || CheckFloor.transform.gameObject.name.Contains("FadingBlock") || CheckFloor.transform.gameObject.name.Contains("RotationBlock"))
            {
                if (CheckFloor.transform.gameObject.name.Contains("RotationBlock") == true)
                {
                    UpdateTotalMoves();
                }
                else
                {
                    MovesSwapped = false;
                }

                if (CheckEnoughMoves(CurrentDirection, 1))
                {
                    transform.LookAt(Target);
                    SetNextBlock(1);
                    TotalDistance -= 1;
                    KeepRunning = true;
                    DeductMove(CurrentDirection, 1);
                }
                else
                {
                    TotalDistance = 0; //Not Enough moves
                }
            }
            else if (CheckFloor.transform.gameObject.name.Contains("SlowBlock") == true)
            {
                if (CheckEnoughMoves(CurrentDirection, 2))
                {
                    transform.LookAt(Target);
                    SetNextBlock(1);
                    TotalDistance -= 1;
                    KeepRunning = true;
                    DeductMove(CurrentDirection, 2);
                }
                else
                {
                    TotalDistance = 0; //Not enough moves
                }

            }
            else if (CheckFloor.transform.gameObject.name.Contains("JumpBlock") == true)
            {
                if (CheckEnoughMoves(CurrentDirection, 1))
                {
                    transform.LookAt(Target);
                    SetNextBlock(2);
                    TotalDistance -= 2;
                    KeepJumping = true;
                    DeductMove(CurrentDirection, 1);
                }
                else
                {
                    TotalDistance = 0;
                }
            }
            else if (CheckFloor.transform.gameObject.name.Contains("FallingBlock") == true)
            {
                Falling = true;
            }
            else if (CheckFloor.transform.gameObject.name.Contains("TeleportBlock") == true)
            {
                int TeleportNumber = int.Parse(CheckFloor.transform.gameObject.name.Substring(CheckFloor.transform.gameObject.name.Length - 1));
                if (TeleportNumber % 2 == 0)
                {
                    Teleporting = true;
                    GameObject TeleportObject = GameObject.Find("TeleportBlock_" + (TeleportNumber + 1).ToString());
                    Vector3 TeleportPosition = TeleportObject.transform.position;
                    StartCoroutine(TeleportPlayer(TeleportPosition));
                }
                else
                {
                    if (CheckEnoughMoves(CurrentDirection, 1))
                    {
                        transform.LookAt(Target);
                        SetNextBlock(1);
                        TotalDistance -= 1;
                        KeepRunning = true;
                        DeductMove(CurrentDirection, 1);
                    }
                    else
                    {
                        TotalDistance = 0; //Not Enough moves
                    }
                }
            }
            else //Unhandled object type
            {
                TotalDistance = 0;
                FinalTargetSet = false;
                Debug.Log("Walked on an object the code doesn't understand");
            }
        }
    }

    bool CheckAxis(GameObject MoveToObject)
    {
        if (transform.position.x != MoveToObject.transform.gameObject.transform.position.x && transform.position.z != MoveToObject.transform.position.z) //Wrong Axis
        {
            return false;
        }
        else if (transform.position.y - 0.55f > MoveToObject.transform.position.y || transform.position.y - 0.55f < MoveToObject.transform.position.y) //If transform is higher or lower than the clicked object, dont go there
        {
            return false;
        }
        else
        {
            return true;
        }
    }

    void SetDirection(GameObject MoveToObject)
    {
        if (transform.position.z < MoveToObject.transform.position.z)
            {
                CurrentDirection = (int)Direction.UP;
            }
        else if (transform.position.z > MoveToObject.transform.position.z)
            {
                CurrentDirection = (int)Direction.DOWN;
            }
        else if (transform.position.x > MoveToObject.transform.position.x)
            {
                CurrentDirection = (int)Direction.LEFT;
            }
        else if (transform.position.x < MoveToObject.transform.position.x)
            {
                CurrentDirection = (int)Direction.RIGHT;
            }
        else
        {
            Debug.Log("Axis is not OK, Player won't move");
        }
    }

    bool CheckEnoughMoves(int CurrentDirection, int MovesNeeded)
    {
        if (BonusMovesActivated == true)
        {
            if (BonusMoves >= MovesNeeded)
            {
                return true;
            }
            else
            {
                ToggleBonusMoves();
            }
        }
        else
        {
            if (CurrentDirection == (int)Direction.UP && MovesUp >= MovesNeeded)
            {
                return true;
            }
            else if (CurrentDirection == (int)Direction.DOWN && MovesDown >= MovesNeeded)
            {
                return true;
            }
            else if (CurrentDirection == (int)Direction.LEFT && MovesLeft >= MovesNeeded)
            {
                return true;
            }
            else if (CurrentDirection == (int)Direction.RIGHT && MovesRight >= MovesNeeded)
            {
                return true;
            }
        }
        Debug.Log("Not enough moves, Player won't move");
        return false;
    }

    void DeductMove(int CurrentDirection,int AmountToRemove)
    {
        if (BonusMovesActivated == true && BonusMoves >= 1)
        {
            BonusMoves -= AmountToRemove;
            UpdateLabels.UpdateBonusMoves(BonusMoves);
            // When Bonus moves are used, moves are not tracked, as if less moves have been made to complete a level

            if (BonusMoves < 1)
            {
                ToggleBonusMoves(); //Also disable the button if after the move we have 0 moves
            }
        }
        else
        {
            if (CurrentDirection == (int)Direction.UP)
            {
                MovesUp -= AmountToRemove;
                UpdateLabels.UpdateUpMoves(MovesUp);
                MoveTracker.UpdateMovesUp();
            }
            else if (CurrentDirection == (int)Direction.DOWN)
            {
                MovesDown -= AmountToRemove;
                UpdateLabels.UpdateDownMoves(MovesDown);
                MoveTracker.UpdateMovesDown();
            }
            else if (CurrentDirection == (int)Direction.LEFT)
            {
                MovesLeft -= AmountToRemove;
                UpdateLabels.UpdateLeftMoves(MovesLeft);
                MoveTracker.UpdateMovesLeft();
            }
            else if (CurrentDirection == (int)Direction.RIGHT)
            {
                MovesRight -= AmountToRemove;
                UpdateLabels.UpdateRightMoves(MovesRight);
                MoveTracker.UpdateMovesRight();
            }
            else
            {
                Debug.Log("Direction uninitiated");
            }
        }
        //Debug.Log("Moves Left: " + MovesUp.ToString() + " " + MovesDown.ToString() + " " + MovesLeft.ToString() + " " + MovesRight.ToString());
    }

    void SetNextBlock(int AmountToMove)
    {
        if (CurrentDirection == (int)Direction.UP)
        {
            NextBlock = new Vector3(transform.position.x, transform.position.y, transform.position.z + AmountToMove);
        }
        else if (CurrentDirection == (int)Direction.DOWN)
        {
            NextBlock = new Vector3(transform.position.x, transform.position.y, transform.position.z - AmountToMove);
        }
        else if (CurrentDirection == (int)Direction.LEFT)
        {
            NextBlock = new Vector3(transform.position.x - AmountToMove, transform.position.y, transform.position.z);
        }
        else if (CurrentDirection == (int)Direction.RIGHT)
        {
            NextBlock = new Vector3(transform.position.x + AmountToMove, transform.position.y, transform.position.z);
        }
    }

    IEnumerator Hop(Vector3 dest, float time)
    {
        if (hopping == true)
        {
            yield break;
        }

        hopping = true;
        var startPos = transform.position;
        var timer = 0.0f;

        while (timer <= 1.0f)
        {
            var height = Mathf.Sin(Mathf.PI * timer) * hopHeight;
            transform.position = Vector3.Lerp(startPos, dest, timer) + Vector3.up * height;

            timer += Time.deltaTime / time;
            if (timer >= 0.75f)
            {
                anim.SetBool("StartFalling", true);
            }
            yield return null;
        }
        transform.position = NextBlock;
        hopping = false;
        KeepJumping = false;
        ChangeStates((int)State.STANDING);
    }

    void CheckFalling()
    {
        RaycastHit ObjectBelow;
        if (!Physics.Raycast(transform.position, new Vector3(0, -5, 0), out ObjectBelow, Mathf.Infinity) && Falling == false && KeepJumping == false)
        {
            transform.parent = null; // If player would be falling because of object pushing and it is its parent briefly, remove it to better simulate falling off
            Falling = true;
            Debug.Log("Player is falling");
            ChangeStates((int)State.FALLING);
        }
    }

    bool Fall()
    {
        if (Falling == true)
        {
            if (fallAngle <= 850)
            {
                fallAngle += 2;
            }
            transform.RotateAround(NextBlock, new Vector3(0, 1.0f, 0), fallAngle * Time.deltaTime);
            NextBlock = new Vector3(NextBlock.x, NextBlock.y - (1.75f * Time.deltaTime), NextBlock.z);
            transform.position = Vector3.MoveTowards(transform.position, NextBlock, (0.002f * fallAngle) * Time.deltaTime);
            if (transform.position.y <= -1.5 && Restarting == false)
            {
                Restarting = true;
                StartCoroutine(RestartDelay());
                Debug.Log("Restarting the level");
            }
            return true;
        }
        return false;
    }

    void ChangeStates(int NewState) //Change states for reference in code
    {
        if (NewState == (int)State.STANDING)
        {
            anim.SetBool("Running", false);
            anim.SetBool("Jumping", false);
        }
        else if (NewState == (int)State.RUNNING)
        {
            anim.SetBool("Running", true);
        }
        else if (NewState == (int)State.JUMPING)
        {
            anim.SetBool("Jumping", true);
        }
        else if (NewState == (int)State.FALLING)
        {
            anim.SetBool("Running", false);
            anim.SetBool("Jumping", false);
        }
        else if (NewState == (int)State.WINNING)
        {
            anim.SetBool("Running", false);
            anim.SetBool("Jumping", false);
            anim.SetBool("Won", true);
        }
        PreviousState = CurrentState;
        CurrentState = NewState;
    }
    void ResumeState()
    {
        CurrentState = PreviousState;
    }

    IEnumerator MakeObjectFall(GameObject ObjectToFallAndDestroy)
    {
        float timer = 0;
        var startPos = ObjectToFallAndDestroy.transform.position;
        ObjectToFallAndDestroy.name = "FallingBlock"; //Setting the name to something different and not LevelBlock because Raycasts mess up while the object is still not destroyed
        if (startPos.y < 0)
        {
            yield break; // Edge case when stepping over object that is already falling
        }
        while (timer < 2.0f)
        {
            ObjectToFallAndDestroy.transform.position = Vector3.Lerp(startPos, new Vector3(startPos.x, startPos.y - 20.0f, startPos.z), timer);
            timer += Time.deltaTime / 2.0f;
            yield return null;
        }
        Destroy(ObjectToFallAndDestroy);
        yield return null;
    }
    IEnumerator TeleportPlayer(Vector3 TeleportPosition)
    {
        float maxSize = 1.0f;
        yield return new WaitForSeconds(0.3f);
        float timer = 0;

            while (maxSize < transform.localScale.x)
            {
                timer += Time.deltaTime / 0.6f;
                transform.localScale -= new Vector3(1, 1, 1) * Time.deltaTime * timer;
                yield return null;
            }
        yield return new WaitForSeconds(0.2f);
        timer = 0;
        maxSize = 3.0f;
        transform.position = new Vector3(TeleportPosition.x, TeleportPosition.y + 0.55f, TeleportPosition.z);
        yield return new WaitForSeconds(0.1f);

        while (maxSize > transform.localScale.x)
        {
            timer += Time.deltaTime / 0.6f;
            transform.localScale += new Vector3(1, 1, 1) * Time.deltaTime * timer;
            yield return null;
        }
        transform.localScale = new Vector3(3, 3, 3);
        Teleporting = false;
        TotalDistance = 0;
    }

    public void UpdateTotalMoves() // Swaps player's moves clockwise (Up->Down->Left->Right)
    {
        if (MovesSwapped == true)
        {
            MovesSwapped = false;
        }
        else
        {
            MovesSwapped = true;

            int TemporaryUp = MovesUp;
            int TemporaryDown = MovesDown;
            int TemporaryLeft = MovesLeft;
            int TemporaryRight = MovesRight;

            MovesUp = TemporaryRight;
            MovesDown = TemporaryUp;
            MovesLeft = TemporaryDown;
            MovesRight = TemporaryLeft;

            UpdateLabels.UpdateUpMoves(MovesUp);
            UpdateLabels.UpdateDownMoves(MovesDown);
            UpdateLabels.UpdateLeftMoves(MovesLeft);
            UpdateLabels.UpdateRightMoves(MovesRight);
        }
    }

    public void ToggleBonusMoves()
    {
        if (BonusMoves < 1)
        {
            Debug.Log("Not enough bonus moves, disabling!");
            BonusMovesActivated = false;
            BonusButton.GetComponent<Image>().color = new Color32(255, 255, 255, 255);
            BonusButton.GetComponent<Button>().interactable = false;
            for (int i = 0; i < DisableMoveButtons.Count; i++)
            {
                DisableMoveButtons[i].GetComponent<Image>().enabled = true;
            }
        }
        else
        {
            if (BonusMovesActivated == false)
            {
                Debug.Log("Disabled earlier");
                BonusMovesActivated = true;
                BonusButton.GetComponent<Image>().color = new Color32(71, 255, 81, 255);
                for (int i = 0; i < DisableMoveButtons.Count; i++)
                {
                    DisableMoveButtons[i].GetComponent<Image>().enabled = false;
                }
            }
            else
            {
                Debug.Log("Disabling now");
                BonusMovesActivated = false;
                BonusButton.GetComponent<Image>().color = new Color32(255, 255, 255, 255);
                for (int i = 0; i < DisableMoveButtons.Count; i++)
                {
                    DisableMoveButtons[i].GetComponent<Image>().enabled = true;
                }
            }
        }

    }
    void ChangeScene(string SceneName)
    {
        SceneManager.LoadScene(SceneName, LoadSceneMode.Single);
    }

    public IEnumerator RestartDelay()
    {
        MoveTracker.UpdateDeaths();
        yield return new WaitForSeconds(1.0f);
        Scene CurrentScene = SceneManager.GetActiveScene();
        GameMaster.DeductLives();
        ChangeScene(CurrentScene.name);
    }
    IEnumerator LevelCompleted()
    {
        MoveTracker.UpdateLevelsCompleted();

        LevelCamera.GetComponent<FollowScript>().LowerCameraLevelEnd();
        //WinningSound.Play(); Add sound for later to play when winning

        Vector3 startingPos = transform.position;
        float elapsedTime = 0;
        float Duration = 1.5f;
        while (elapsedTime < Duration)
        {
            Vector3 targetDirection = LevelCamera.transform.position - transform.position;
            Vector3 newDirection = Vector3.RotateTowards(transform.forward, new Vector3(targetDirection.x,0,targetDirection.z), (elapsedTime / Duration), 0.0f);
            transform.rotation = Quaternion.LookRotation(newDirection);
            elapsedTime += Time.deltaTime;
            yield return null;

        }
        ChangeStates((int)State.WINNING); // Cancel any animations after level finishes
        yield return new WaitForSeconds(4.5f);
        ChangeScene("LevelSelect");
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.name == "WinObject")
        {
            other.gameObject.GetComponent<AnimationScript>().Raise = true;
            PauseCharacter = true; //Prevents character from moving and tells the game the level is finished
            LevelFinished = true; //Prevents character from moving and tells the game the level is finished
            ChangeStates((int)State.STANDING);
            transform.position = NextBlock;
            StartCoroutine(LevelCompleted());
        }
        if (other.name == "BonusStar")
        {
            Destroy(other.gameObject);
            MoveTracker.UpdateBonusStar();
        }
        if (other.name == "EnemyBlock")
        {
            RaycastHit ObjectInFront;
            if (Physics.Raycast(transform.position, new Vector3(0, 0, 1), out ObjectInFront, Mathf.Infinity) ||
                Physics.Raycast(transform.position, new Vector3(0, 0, -1), out ObjectInFront, Mathf.Infinity))
            {
                KeepRunning = false;
                FinalTargetSet = false;
                transform.parent = other.transform; //Make it seem that the object is pushing the player
            }
            else
            {
                PauseCharacter = true;
                ChangeStates((int)State.STANDING);
            }

        }
        if (other.name.Contains("BonusMove"))
        {
            BonusMoves = other.GetComponent<ExtraMoves>().BonusMoves;
            UpdateLabels.UpdateBonusMoves(BonusMoves);
            BonusButton.GetComponent<Button>().interactable = true; //By default it is set to interactable = false when there are no bonus moves available
            Destroy(other.gameObject);
        }
    }

}
