﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LevelProgressTracker : MonoBehaviour
{
    public GameObject StatsPanel;
    public GameObject Mask;
    public Text TotalMovesText;
    public Text LeastMovesText;
    public Text TotalDeathsText;
    public Text LevelNameText;
    public Text BestPossibleText;
    public Image[] Stars;
    public Button BackButton;
    GameController GameMaster;
    Image MaskAlpha;
    Vector3 HiddenPosition;
    Vector3 VisiblePosition;
    private float journeyLength;
    float cumulativeAlpha;
    bool PanelMoving = false;
    public LevelData LevelDataScript = new LevelData();
    LevelDataStructure LevelStructure;
    private Outline outline;

    private void Start()
    {
        HiddenPosition = new Vector3(0f, 850f, 0f);
        VisiblePosition = new Vector3(0f, 0f, 0f);
        MaskAlpha = Mask.GetComponent<Image>();
        GameMaster = GameObject.Find("GameMaster").GetComponent<GameController>();
        outline = BestPossibleText.GetComponent<Outline>(); // OUTLINE HOOK FOR BEST POSSIBLE MOVES TEXT
    }

    public void ShowStats(Text LevelName)
    {
        if (PanelMoving == false)
        {
            PanelMoving = true;
            MaskAlpha.raycastTarget = true;
            cumulativeAlpha = 0;
            StartCoroutine(MoveOnScreen());
            LoadPanelStats(int.Parse(LevelName.text));
        }
    }

    public void HideStats()
    {
        if (PanelMoving == false)
        {
            PanelMoving = true;
            cumulativeAlpha = 180;
            StartCoroutine(MoveOffScreen());
        }
    }

    IEnumerator MoveOnScreen()
    {
        float elapsedTime = 0;
        float waitTime = 0.5f;
        float AlphaIncrease;
        while (elapsedTime < waitTime)
        {
            StatsPanel.transform.localPosition = Vector3.Lerp(HiddenPosition, VisiblePosition, (elapsedTime / waitTime));
            AlphaIncrease = (Time.deltaTime / waitTime) * 180; //180 is how dark we want the mask to be
            cumulativeAlpha += AlphaIncrease;
            MaskAlpha.color = new Color32(0, 0, 0, (byte)cumulativeAlpha);
            elapsedTime += Time.deltaTime;
            // Yield here
            yield return null;
        }
        // Make sure we got there
        StatsPanel.transform.localPosition = VisiblePosition;
        PanelMoving = false; //allow panel to be moveable again
        yield return null;

    }

    IEnumerator MoveOffScreen()
    {
        float elapsedTime = 0;
        float waitTime = 0.5f;
        float AlphaIncrease;
        while (elapsedTime < waitTime)
        {
            StatsPanel.transform.localPosition = Vector3.Lerp(VisiblePosition, HiddenPosition, (elapsedTime / waitTime));
            AlphaIncrease = (Time.deltaTime / waitTime) * 180; //180 is how dark we want the mask to be
            cumulativeAlpha -= AlphaIncrease;
            if (cumulativeAlpha <= 0)
            {
                cumulativeAlpha = 0; //If it accidentally goes to negative, (byte)cumulativeAlpha changes to 1.0 (completely black)
            }
            MaskAlpha.color = new Color32(0, 0, 0, (byte)cumulativeAlpha);
            elapsedTime += Time.deltaTime;
            // Yield here
            yield return null;
        }
        // Make sure we got there
        StatsPanel.transform.localPosition = HiddenPosition;
        MaskAlpha.raycastTarget = false;
        PanelMoving = false; //allow panel to be moveable again
        yield return null;
    }

    void LoadPanelStats(int LevelName)
    {
        LevelStructure = LevelDataScript.GetType().GetField("Level" + LevelName).GetValue(LevelDataScript) as LevelDataStructure;
        LevelNameText.text = "Level " + LevelName.ToString();
        TotalMovesText.text = GameMaster.LevelList[LevelName].TotalMoves.ToString();
        LeastMovesText.text = GameMaster.LevelList[LevelName].LeastMovesMade.ToString();
        TotalDeathsText.text = GameMaster.LevelList[LevelName].TotalDeaths.ToString();
        if (!(GameMaster.LevelList[LevelName].TotalStars <= 0))
        {
            outline.enabled = true;
            BestPossibleText.text = "Best Possible: " + LevelStructure.LeastMoves.ToString();
            if (GameMaster.LevelList[LevelName].LeastMovesMade <= LevelStructure.LeastMoves)
            {
                outline.effectColor = new Color(12, 210, 0, 64);
            }
            else
            {
                outline.effectColor = new Color(255, 0, 0, 64);
            }
        }
        else
        {
            BestPossibleText.text = "Best Possible: ???";
            outline.enabled = false;
        }

        for (int i = 0;i < 3;i++)
        {
            Stars[i].fillAmount = 0; // Clear all star fills before opening a new panel
        }

        for (int i = 0;i < GameMaster.LevelList[LevelName].TotalStars;i++)
        {
            Stars[i].fillAmount = 1; // Fill stars relevant to the current level
        }

        if (GameMaster.Lives <= 0)
        {
            BackButton.GetComponent<Button>().interactable = false;
        }
        else
        {
            BackButton.GetComponent<Button>().interactable = true;
        }

    }
}
