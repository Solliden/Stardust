﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Fading_Script : MonoBehaviour
{
    int MaxBlinks = 8; // 10 Maximum
    int TotalBlinks;
    float AnimationTime = 0.25f;
    float Frequency_Low = 1f;
    float Frequency_High = 2.5f;
    float alpha = 1.0f;
    float DissapearTime = 1.5f;
    bool NumberGenerated = false;
    bool AnimationStarted = false;
    bool FadeInProgress = false;
    float WaitTime;
    float TimePassed;
    float PartialDuration;
    float PartialAlpha;
    bool ObjectFaded = false;

    private void Start()
    {
        TotalBlinks = MaxBlinks;
    }

    void Update()
    {
        TimePassed += Time.deltaTime;

        if (NumberGenerated == false)
        {
            WaitTime = Random.Range(Frequency_Low, Frequency_High);
        }

        if (TimePassed >= WaitTime)
        {
            AnimationStarted = true;
        }

        if (AnimationStarted == true)
        {
            if (TotalBlinks >= 1)
            {
                if (FadeInProgress == false)
                {
                    FadeInProgress = true;
                    TotalBlinks -= 1;
                    PartialDuration = (AnimationTime / MaxBlinks) * TotalBlinks;
                    PartialAlpha = (alpha / MaxBlinks) * TotalBlinks;
                    StartCoroutine(FadeInAndOut(PartialDuration, PartialAlpha));
                }
            }
            else if (TotalBlinks < 1 && FadeInProgress == false) // Reset animation
            {
                AnimationStarted = false;
                NumberGenerated = false;
                TimePassed = 0f;
                TotalBlinks = MaxBlinks;
            }
        }
    }

    void ChangeAlpha(Material mat, float alphaVal)
    {
        Color oldColor = mat.color;
        Color newColor = new Color(oldColor.r, oldColor.g, oldColor.b, alphaVal);
        mat.SetColor("_Color", newColor);
    }

    IEnumerator FadeInAndOut(float Duration,float Alpha)
    {
        if (TotalBlinks >= 1)
        {
            ChangeAlpha(gameObject.GetComponent<Renderer>().material, Alpha);
            yield return new WaitForSeconds(Duration);
            ChangeAlpha(gameObject.GetComponent<Renderer>().material, (1.0f + Alpha) / 2);
            yield return new WaitForSeconds(Duration);
            FadeInProgress = false;
        }
        else
        {
            gameObject.GetComponent<MeshRenderer>().enabled = false;
            gameObject.GetComponent<BoxCollider>().enabled = false;
            ObjectFaded = true;
            yield return new WaitForSeconds(DissapearTime); // How long the block has dissapeared for
            gameObject.GetComponent<Renderer>().enabled = true;
            gameObject.GetComponent<BoxCollider>().enabled = true;
            ChangeAlpha(gameObject.GetComponent<Renderer>().material, alpha);
            ObjectFaded = true;
            FadeInProgress = false;
        }
    }
}
