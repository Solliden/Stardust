﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using System;

public class MovesTracker : MonoBehaviour
{
    GameController GameMaster;

    int LevelsCompleted;
    int TotalMoves;
    int TotalMovesUp;
    int TotalMovesDown;
    int TotalMovesLeft;
    int TotalMovesRight;
    int TotalDeaths;
    int TotalStars;


    string CurrentLevelName;
    int CurrentLevel;

    int CurrentTotalMoves = 0;
    int CurrentDeaths = 0;
    bool BonusStarCollected = false;
    int TotalLevelStars;
    private void Start() // Initialise loaded values from main script
    {
        GameMaster = GameObject.Find("GameMaster").GetComponent<GameController>();
        LevelsCompleted = GameMaster.LevelsCompleted;
        TotalMoves = GameMaster.TotalMoves;
        TotalMovesUp = GameMaster.TotalMovesUp;
        TotalMovesDown = GameMaster.TotalMovesDown;
        TotalMovesLeft = GameMaster.TotalMovesLeft;
        TotalMovesRight = GameMaster.TotalMovesRight;
        TotalDeaths = GameMaster.TotalDeaths;
        CurrentLevelName = SceneManager.GetActiveScene().name;
        CurrentLevel = Int32.Parse(CurrentLevelName.Substring(5, CurrentLevelName.Length - 5));
    }

    public void UpdateMovesUp()
    {
        TotalMovesUp += 1;
        TotalMoves += 1;
        CurrentTotalMoves += 1;
    }
    public void UpdateMovesDown()
    {
        TotalMovesDown += 1;
        TotalMoves += 1;
        CurrentTotalMoves += 1;
    }
    public void UpdateMovesLeft()
    {
        TotalMovesLeft += 1;
        TotalMoves += 1;
        CurrentTotalMoves += 1;
    }
    public void UpdateMovesRight()
    {
        TotalMovesRight += 1;
        TotalMoves += 1;
        CurrentTotalMoves += 1;
    }
    public void UpdateDeaths()
    {
        TotalDeaths += 1;
        CurrentDeaths += 1;
        UpdateStats();
    }

    public void UpdateBonusStar()
    {
        BonusStarCollected = true;

        if (BonusStarCollected != GameMaster.LevelList[CurrentLevel].BonusStarCollected)
        {
            GameMaster.LevelList[CurrentLevel].BonusStarCollected = BonusStarCollected;
        }
    }

    public void UpdateLevelsCompleted()
    {
        if (CurrentLevel > LevelsCompleted)
        {
            LevelsCompleted += 1;
            GameMaster.LevelList[CurrentLevel].LevelCompleted = true;
        }
        if (CurrentTotalMoves < GameMaster.LevelList[CurrentLevel].LeastMovesMade || GameMaster.LevelList[CurrentLevel].LeastMovesMade == 0)
        {
            GameMaster.LevelList[CurrentLevel].LeastMovesMade = CurrentTotalMoves;
        }

        if (CurrentTotalMoves <= GameMaster.CurrentLevel.LeastMovesAndBonusStar)
        {

            GameMaster.LevelList[CurrentLevel].CorrectLeastMoves = true;
            GameMaster.LevelList[CurrentLevel].CorrectLeastMovesWithStar = true;
        }
        else if (CurrentTotalMoves <= GameMaster.CurrentLevel.LeastMoves)
        {
            GameMaster.LevelList[CurrentLevel].CorrectLeastMoves = true;
        }

        // Code to update achievement stars
        TotalLevelStars += 1; // 1 Star for completing the level
        if (GameMaster.LevelList[CurrentLevel].BonusStarCollected == true)
        {
            TotalLevelStars += 1; // 1 Star for collecting the bonus star
        }
        if (GameMaster.LevelList[CurrentLevel].CorrectLeastMoves == true)
        {
            TotalLevelStars += 1; // 1 Star for completing the level in least amount of moves
        }

        if (TotalLevelStars > 3)
        {
            Debug.Log("Bug occured");
            TotalLevelStars = 3; //In case somehow it gets set to higher than 3, would be a bug
        }

        UpdateStats();
    }

    //Call this from UpdateDeaths and UpdateLevelsCompleted only
    //Stats should only update when a player dies or completes the level, not if crashes, Alt+f4's or leaves the level instance
    void UpdateStats() 
    {
        GameMaster.TotalMoves = TotalMoves;
        GameMaster.TotalMovesUp = TotalMovesUp;
        GameMaster.TotalMovesDown = TotalMovesDown;
        GameMaster.TotalMovesLeft = TotalMovesLeft;
        GameMaster.TotalMovesRight = TotalMovesRight;
        GameMaster.TotalDeaths = TotalDeaths;
        GameMaster.LevelsCompleted = LevelsCompleted;
        GameMaster.LevelList[CurrentLevel].TotalMoves += CurrentTotalMoves;
        GameMaster.LevelList[CurrentLevel].TotalDeaths += CurrentDeaths;
        if (GameMaster.LevelList[CurrentLevel].TotalStars < TotalLevelStars)
        {
            GameMaster.TotalStars += TotalLevelStars - GameMaster.LevelList[CurrentLevel].TotalStars; //Add stars to the total
            GameMaster.LevelList[CurrentLevel].TotalStars = TotalLevelStars;
        }
        GameMaster.SaveGameData();
        GameMaster.SaveLevelInformation();
    }
}
